CREATE TABLE IF NOT EXISTS `gladiator_player_perks` (
  `player_id` INT(11) NOT NULL,
  `gladiator_half_hearts` INT(11) NOT NULL DEFAULT 40,
  `slave_bow_chance` INT(11) NOT NULL DEFAULT 20,
  `slave_extra_chance` INT(11) NOT NULL DEFAULT 20,
  `slave_potion_chance` INT(11) NOT NULL DEFAULT 20,
  `enchant_chance` INT(11) NOT NULL DEFAULT 20,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_gladiator_player_perks_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;