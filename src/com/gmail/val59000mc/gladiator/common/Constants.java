package com.gmail.val59000mc.gladiator.common;

public class Constants {
	public final static String GLADIATOR_TEAM_NAME = "Gladiateur";
	public final static String SLAVES_TEAM_NAME = "Esclaves";
	
	public final static int SLAVE_ENCHANT_CHANCE = 50;
	
	public static final int ROUND_DURATION = 90;
	public static final int SLAVE_RESPAWN_TIME = 15;
	
	public static double REWARD_KILL_GLADIATOR = 30;
	public static double REWARD_KILL_SLAVE = 3;
	public static double REWARD_WIN_ROUND_AS_GLADIATOR = 8;
	public static double REWARD_WIN_ROUND_AS_SLAVE = 15;
}
