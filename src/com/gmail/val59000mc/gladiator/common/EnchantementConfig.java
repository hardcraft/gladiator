package com.gmail.val59000mc.gladiator.common;

import org.bukkit.enchantments.Enchantment;

public class EnchantementConfig {
	
	private Enchantment enchantement;
	private Integer level;
	public EnchantementConfig(Enchantment enchantement, Integer level) {
		super();
		this.enchantement = enchantement;
		this.level = level;
	}
	public Enchantment getEnchantement() {
		return enchantement;
	}
	public Integer getLevel() {
		return level;
	}
	
	
}
