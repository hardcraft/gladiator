package com.gmail.val59000mc.gladiator.common;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.gladiator.players.GladiatorPlayer;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;

public class GladiatorItems {

	private  HCGameAPI api;

	private List<ItemStack> gladiatorSwords;
	private List<EnchantementConfig> gladiatorSwordsEnchantements;
	private List<ItemStack> gladiatorBows;
	private List<EnchantementConfig> gladiatorBowsEnchantements;
	private List<ItemStack> gladiatorPotions;
	private List<ItemStack> gladiatorHelmets;
	private List<ItemStack> gladiatorChestplates;
	private List<ItemStack> gladiatorleggings;
	private List<ItemStack> gladiatorBoots;
	private List<EnchantementConfig> gladiatorArmorEnchantements;
	private List<ItemStack> gladiatorExtras;

	private List<ItemStack> slavesSwords;
	private List<EnchantementConfig> slavesSwordsEnchantements;
	private List<ItemStack> slavesBows;
	private List<EnchantementConfig> slavesBowsEnchantements;
	private List<ItemStack> slavesPotions;
	private List<ItemStack> slavesHelmets;
	private List<ItemStack> slavesChestplates;
	private List<ItemStack> slavesleggings;
	private List<ItemStack> slavesBoots;
	private List<EnchantementConfig> slavesArmorEnchantements;
	private List<ItemStack> slavesExtras;
	
	public GladiatorItems(HCGameAPI api) {
		this.api = api;
	}

	public void setup(){
		
		// gladiator		
		this.gladiatorSwords = Lists.newArrayList(
			new ItemStack(Material.IRON_SWORD),
			new ItemStack(Material.DIAMOND_SWORD)
		);
		
		this.gladiatorSwordsEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.DAMAGE_ALL, 1),
			new EnchantementConfig(Enchantment.DAMAGE_ALL, 2),
			new EnchantementConfig(Enchantment.KNOCKBACK, 1),
			new EnchantementConfig(Enchantment.KNOCKBACK, 2),
			new EnchantementConfig(Enchantment.FIRE_ASPECT, 1),
			new EnchantementConfig(Enchantment.FIRE_ASPECT, 2)
		);
		
		this.gladiatorBows = Lists.newArrayList(new ItemStack(Material.BOW));
		
		this.gladiatorBowsEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.ARROW_DAMAGE, 1),	
			new EnchantementConfig(Enchantment.ARROW_DAMAGE, 2),	
			new EnchantementConfig(Enchantment.ARROW_DAMAGE, 3),	
			new EnchantementConfig(Enchantment.ARROW_FIRE, 1),	
			new EnchantementConfig(Enchantment.ARROW_KNOCKBACK, 1),	
			new EnchantementConfig(Enchantment.ARROW_KNOCKBACK, 2)	
		);
		
		this.gladiatorPotions = Lists.newArrayList(
			new ItemStack(Material.POTION, 1, (short) 8257),
			new ItemStack(Material.POTION, 1, (short) 8258),
			new ItemStack(Material.POTION, 1, (short) 8226),
			new ItemStack(Material.POTION, 1, (short) 8227),
			new ItemStack(Material.POTION, 1, (short) 8229),
			new ItemStack(Material.POTION, 1, (short) 16420),
			new ItemStack(Material.POTION, 1, (short) 16421),
			new ItemStack(Material.POTION, 1, (short) 16428)
		);

		this.gladiatorHelmets = Lists.newArrayList(
//			new ItemStack(Material.IRON_HELMET),
			new ItemStack(Material.DIAMOND_HELMET)
		);

		this.gladiatorChestplates = Lists.newArrayList(
//			new ItemStack(Material.IRON_CHESTPLATE),
			new ItemStack(Material.DIAMOND_CHESTPLATE)
		);

		this.gladiatorleggings = Lists.newArrayList(
//			new ItemStack(Material.IRON_LEGGINGS),
			new ItemStack(Material.DIAMOND_LEGGINGS)
		);

		this.gladiatorBoots = Lists.newArrayList(
//			new ItemStack(Material.IRON_BOOTS),
			new ItemStack(Material.DIAMOND_BOOTS)
		);
		
		this.gladiatorArmorEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.PROTECTION_ENVIRONMENTAL, 2),	
			new EnchantementConfig(Enchantment.PROTECTION_ENVIRONMENTAL, 3),	
			new EnchantementConfig(Enchantment.PROTECTION_PROJECTILE, 2),	
			new EnchantementConfig(Enchantment.PROTECTION_PROJECTILE, 3),	
			new EnchantementConfig(Enchantment.THORNS, 1),	
			new EnchantementConfig(Enchantment.THORNS, 2),	
			new EnchantementConfig(Enchantment.PROTECTION_EXPLOSIONS, 2),	
			new EnchantementConfig(Enchantment.PROTECTION_EXPLOSIONS, 3)
		);

		this.gladiatorExtras = Lists.newArrayList(
			new ItemStack(Material.FLINT_AND_STEEL, 1, (short) 10),
			new ItemStack(Material.FISHING_ROD, 1, (short) 15),
			new ItemStack(Material.WATER_BUCKET, 1),
			new ItemStack(Material.LAVA_BUCKET, 1),
			new ItemStack(Material.BUCKET, 1),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 61),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 50),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 68),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 60),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 66),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 56),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 62),
			new ItemStack(Material.SNOW_BALL, 16),
			new ItemStack(Material.GOLDEN_APPLE, 1)
		);
		
		// slaves
		slavesSwords = Lists.newArrayList(
			new ItemStack(Material.WOOD_SWORD),
			new ItemStack(Material.GOLD_SWORD),
			new ItemStack(Material.STONE_SWORD)
		);
		
		this.slavesSwordsEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.DAMAGE_ALL, 1),
			new EnchantementConfig(Enchantment.KNOCKBACK, 1),
			new EnchantementConfig(Enchantment.DURABILITY, 1),
			new EnchantementConfig(Enchantment.DURABILITY, 2),
			new EnchantementConfig(Enchantment.DURABILITY, 3),
			new EnchantementConfig(Enchantment.DAMAGE_UNDEAD, 1),
			new EnchantementConfig(Enchantment.DAMAGE_ARTHROPODS, 1),
			new EnchantementConfig(Enchantment.LOOT_BONUS_MOBS, 1)
		);
		
		this.slavesBows = Lists.newArrayList(new ItemStack(Material.BOW));
		
		this.slavesBowsEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.ARROW_DAMAGE, 1),	
			new EnchantementConfig(Enchantment.DURABILITY, 2),	
			new EnchantementConfig(Enchantment.ARROW_KNOCKBACK, 1),	
			new EnchantementConfig(Enchantment.ARROW_FIRE, 1)
		);
		
		this.slavesPotions = Lists.newArrayList(
			new ItemStack(Material.POTION, 1, (short) 16385),
			new ItemStack(Material.POTION, 1, (short) 16388),
			new ItemStack(Material.POTION, 1, (short) 8194),
			new ItemStack(Material.POTION, 1, (short) 8261),
			new ItemStack(Material.POTION, 1, (short) 8203),
			new ItemStack(Material.POTION, 1, (short) 8238)
		);
		
		this.slavesHelmets = Lists.newArrayList(
			new ItemStack(Material.LEATHER_HELMET),	
			new ItemStack(Material.CHAINMAIL_HELMET),	
			new ItemStack(Material.GOLD_HELMET)
		);

		this.slavesChestplates = Lists.newArrayList(
			new ItemStack(Material.LEATHER_CHESTPLATE),
			new ItemStack(Material.CHAINMAIL_CHESTPLATE),
			new ItemStack(Material.GOLD_CHESTPLATE)
		);

		this.slavesleggings = Lists.newArrayList(
			new ItemStack(Material.LEATHER_LEGGINGS),
			new ItemStack(Material.CHAINMAIL_LEGGINGS),
			new ItemStack(Material.GOLD_LEGGINGS)
		);

		this.slavesBoots = Lists.newArrayList(
			new ItemStack(Material.LEATHER_BOOTS),
			new ItemStack(Material.CHAINMAIL_BOOTS),
			new ItemStack(Material.GOLD_BOOTS)
		);
		
		this.slavesArmorEnchantements = Lists.newArrayList(
			new EnchantementConfig(Enchantment.PROTECTION_ENVIRONMENTAL, 1),	
			new EnchantementConfig(Enchantment.PROTECTION_PROJECTILE, 1),	
			new EnchantementConfig(Enchantment.PROTECTION_FIRE, 1),	
			new EnchantementConfig(Enchantment.PROTECTION_EXPLOSIONS, 1),	
			new EnchantementConfig(Enchantment.DURABILITY, 2)
		);

		this.slavesExtras = Lists.newArrayList(
			new ItemStack(Material.FLINT_AND_STEEL, 1, (short) 25),
			new ItemStack(Material.FISHING_ROD, 1, (short) 15),
			new ItemStack(Material.WATER_BUCKET, 1),
			new ItemStack(Material.LAVA_BUCKET, 1),
			new ItemStack(Material.BUCKET, 1),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 61),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 50),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 68),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 60),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 66),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 56),
			new ItemStack(Material.MONSTER_EGG, 1, (short) 62),
			new ItemStack(Material.SNOW_BALL, 3),
			new ItemStack(Material.SNOW_BALL, 5),
			new ItemStack(Material.SNOW_BALL, 10)
		);
	}
	
	private boolean hasItemWithChance(int chance){
		return Randoms.randomInteger(1, 100) <= chance;
	}
	
	private ItemStack getRandomItem(List<ItemStack> items){
		return items.get(Randoms.randomInteger(0, items.size()-1));
	}
	
	private List<ItemStack> getRandomItems(List<ItemStack> items, int amount){
		List<ItemStack> randomItems = new ArrayList<ItemStack>();
		for(int i=0; i<amount ; i++){
			randomItems.add(getRandomItem(items));
		}
		return randomItems;
	}
	
	private ItemStack addRandomEnchant(ItemStack item, List<EnchantementConfig> enchants, int chanceToGetAnEnchant){
		if(Randoms.randomInteger(1, 100) <= chanceToGetAnEnchant){
			EnchantementConfig ench = enchants.get(Randoms.randomInteger(0, enchants.size()-1));
			item.addEnchantment(ench.getEnchantement(), ench.getLevel());
		}
		return item;
	}
	
	public List<ItemStack> getGladiatorArmor(GladiatorPlayer gladiatorPlayer){
		return Lists.newArrayList(
			addRandomEnchant(getRandomItem(gladiatorHelmets), gladiatorArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(gladiatorChestplates), gladiatorArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(gladiatorleggings), gladiatorArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(gladiatorBoots), gladiatorArmorEnchantements, gladiatorPlayer.getEnchantChance())
		);
	}
	

	public List<ItemStack> getGladiatorItems(GladiatorPlayer gladiatorPlayer){
		List<ItemStack> items = new ArrayList<ItemStack>();
		items.add(addRandomEnchant(getRandomItem(gladiatorSwords), gladiatorSwordsEnchantements, gladiatorPlayer.getEnchantChance()));
		items.add(addRandomEnchant(getRandomItem(gladiatorBows), gladiatorBowsEnchantements, gladiatorPlayer.getEnchantChance()));
		items.add(new ItemStack(Material.COOKED_BEEF, 8));
		items.addAll(getRandomItems(gladiatorPotions, 2));
		items.addAll(getRandomItems(gladiatorExtras, 4));
		items.add(new ItemStack(Material.ARROW, 32));
		return items;
	}

	public List<ItemStack> getSlaveArmor(GladiatorPlayer gladiatorPlayer){
		return Lists.newArrayList(
			addRandomEnchant(getRandomItem(slavesHelmets), slavesArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(slavesChestplates), slavesArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(slavesleggings), slavesArmorEnchantements, gladiatorPlayer.getEnchantChance()),
			addRandomEnchant(getRandomItem(slavesBoots), slavesArmorEnchantements, gladiatorPlayer.getEnchantChance())
		);
	}

	public List<ItemStack> getSlaveItems(GladiatorPlayer gladiatorPlayer){
		List<ItemStack> items = new ArrayList<ItemStack>();
		items.add(addRandomEnchant(getRandomItem(slavesSwords), slavesSwordsEnchantements, gladiatorPlayer.getEnchantChance()));
		
		if(hasItemWithChance(gladiatorPlayer.getSlaveBowChance())){
			items.add(addRandomEnchant(getRandomItem(slavesBows), slavesBowsEnchantements, gladiatorPlayer.getEnchantChance()));
			items.add(new ItemStack(Material.ARROW, 16));
		}
		
		items.add(new ItemStack(Material.COOKED_BEEF, 8));
		
		if(hasItemWithChance(gladiatorPlayer.getSlavePotionChance())){
			items.add(getRandomItem(slavesPotions));
		}
		
		if(hasItemWithChance(gladiatorPlayer.getSlaveExtraChance())){
			items.add(getRandomItem(slavesExtras));
		}

		return items;
	}
	
}
