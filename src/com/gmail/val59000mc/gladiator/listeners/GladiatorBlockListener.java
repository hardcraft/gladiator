package com.gmail.val59000mc.gladiator.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class GladiatorBlockListener extends HCListener{

	@EventHandler
	public void onBreakBlock(BlockBreakEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void emptiesBucket(PlayerBucketEmptyEvent e){
		if(e.getBlockClicked().getLocation().getY() - 2 >= e.getPlayer().getLocation().getY()){
			e.setCancelled(true);
		}
	}
}
