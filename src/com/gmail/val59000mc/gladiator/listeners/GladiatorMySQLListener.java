package com.gmail.val59000mc.gladiator.listeners;

import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.gladiator.players.GladiatorPlayer;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Logger;

public class GladiatorMySQLListener extends HCListener{

	// Queries
	private String createGladiatorPlayerSQL;
	private String createGladiatorPlayerStatsSQL;
	private String createGladiatorPlayerPerksSQL;
	private String insertGladiatorPlayerSQL;
	private String insertGladiatorPlayerStatsSQL;
	private String insertGladiatorPlayerPerksSQL;
	private String updateGladiatorPlayerGlobalStatsSQL;
	private String updateGladiatorPlayerStatsSQL;
	private String selectGladiatorPlayerPerksSQL;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createGladiatorPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_gladiator_player.sql");
			createGladiatorPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_gladiator_player_stats.sql");
			createGladiatorPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_gladiator_player_perks.sql");
			insertGladiatorPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_gladiator_player.sql");
			insertGladiatorPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_gladiator_player_stats.sql");
			insertGladiatorPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_gladiator_player_perks.sql");
			updateGladiatorPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_gladiator_player_global_stats.sql");
			updateGladiatorPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_gladiator_player_stats.sql");
			selectGladiatorPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/select_gladiator_player_perks.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
						sql.execute(sql.prepareStatement(createGladiatorPlayerSQL));
						
						sql.execute(sql.prepareStatement(createGladiatorPlayerStatsSQL));
						
						sql.execute(sql.prepareStatement(createGladiatorPlayerPerksSQL));
					
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for Gladiator stats or perks");
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					String id = String.valueOf(e.getHcPlayer().getId());

					try {
						
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertGladiatorPlayerSQL, id));
						
						// Insert ctf player stats if not exists
						sql.execute(sql.prepareStatement(insertGladiatorPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
						sql.execute(sql.prepareStatement(insertGladiatorPlayerPerksSQL, id));
						
						CachedRowSet perks = sql.query(sql.prepareStatement(selectGladiatorPlayerPerksSQL, id));
						perks.first();
						GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) e.getHcPlayer();
						gladiatorPlayer.setGladiatorHalfHearts(perks.getInt("gladiator_half_hearts"));
						gladiatorPlayer.setEnchantChance(perks.getInt("enchant_chance"));
						gladiatorPlayer.setSlaveExtraChance(perks.getInt("slave_extra_chance"));
						gladiatorPlayer.setSlaveBowChance(perks.getInt("slave_bow_chance"));
						gladiatorPlayer.setSlavePotionChance(perks.getInt("slave_potion_chance"));
					} catch (SQLException e1) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						// Update ctf player global stats
						sql.execute(sql.prepareStatement(updateGladiatorPlayerGlobalStatsSQL,
								String.valueOf(gladiatorPlayer.getGladiatorKilled()),
								String.valueOf(gladiatorPlayer.getSlavesKilled()),
								String.valueOf(gladiatorPlayer.getRoundsWonAsGladiator()),
								String.valueOf(gladiatorPlayer.getRoundsWonAsSlave()),
								String.valueOf(gladiatorPlayer.getTimePlayed()),
								String.valueOf(gladiatorPlayer.getKills()),
								String.valueOf(gladiatorPlayer.getDeaths()),
								String.valueOf(gladiatorPlayer.getMoney()),
								String.valueOf(gladiatorPlayer.getId())
						));
						
	
						// Update ctf player stats
						sql.execute(sql.prepareStatement(updateGladiatorPlayerStatsSQL,
								String.valueOf(gladiatorPlayer.getGladiatorKilled()),
								String.valueOf(gladiatorPlayer.getSlavesKilled()),
								String.valueOf(gladiatorPlayer.getRoundsWonAsGladiator()),
								String.valueOf(gladiatorPlayer.getRoundsWonAsSlave()),
								String.valueOf(gladiatorPlayer.getTimePlayed()),
								String.valueOf(gladiatorPlayer.getKills()),
								String.valueOf(gladiatorPlayer.getDeaths()),
								String.valueOf(gladiatorPlayer.getMoney()),
								String.valueOf(gladiatorPlayer.getId()),
								sql.getMonth(),
								sql.getYear()
						));
						
					}catch(SQLException e){
						Logger.severe("Couldnt update Gladiator player stats for player="+gladiatorPlayer.getName()+" uuid="+gladiatorPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}
