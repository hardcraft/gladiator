package com.gmail.val59000mc.gladiator.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerEliminatedEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerSpectateEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerVanishedEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;

public class GladiatorPlayerQuitListener extends HCListener{

	
	private void eliminateIfPlaying(HCPlayer hcPlayer) {
		if(hcPlayer.is(PlayerState.PLAYING)){
			getPmApi().eliminatePlayer(hcPlayer);
		}
	}
	
	@EventHandler
	public void onQuit(HCPlayerQuitEvent e){
		eliminateIfPlaying(e.getHcPlayer());
	}

	@EventHandler
	public void onKilled(HCPlayerKilledByPlayerEvent e){
		eliminateIfPlaying(e.getHCKilled());
	}
	
	@EventHandler
	public void onDeath(HCPlayerKilledEvent e){
		eliminateIfPlaying(e.getHcPlayer());
	}
	
	@EventHandler
	public void onSpec(HCPlayerSpectateEvent e){
		if(getApi().is(GameState.PLAYING)){
			Bukkit.getPluginManager().callEvent(new HCPlayerEliminatedEvent(getApi(), e.getHcPlayer()));
		}
	}
	
	@EventHandler
	public void onSpec(HCPlayerVanishedEvent e){
		if(getApi().is(GameState.PLAYING)){
			Bukkit.getPluginManager().callEvent(new HCPlayerEliminatedEvent(getApi(), e.getHcPlayer()));
		}
	}

}
