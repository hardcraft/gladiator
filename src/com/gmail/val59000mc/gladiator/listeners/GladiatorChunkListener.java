package com.gmail.val59000mc.gladiator.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.ChunkUnloadEvent;

import com.gmail.val59000mc.gladiator.common.Constants;
import com.gmail.val59000mc.gladiator.players.GladiatorTeam;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterPlayEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class GladiatorChunkListener extends HCListener{

	private List<ChunkCoordinates> chunks;
	
	public GladiatorChunkListener(){
		this.chunks = new ArrayList<ChunkCoordinates>();
	}

	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		GladiatorTeam gladiator = (GladiatorTeam) getPmApi().getHCTeam(Constants.GLADIATOR_TEAM_NAME);
		registerUnloadableChunks(gladiator.getSpawnpoints());
		
		GladiatorTeam slaves = (GladiatorTeam) getPmApi().getHCTeam(Constants.SLAVES_TEAM_NAME);
		registerUnloadableChunks(slaves.getSpawnpoints());
		
	}
	
	private void registerUnloadableChunks(List<Location> spawnpoints) {
		for(Location location : spawnpoints){
			Chunk chunk = location.getChunk();
			this.chunks.add(new ChunkCoordinates(location.getChunk()));
			chunk.load();
		}
	}

	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent e){
		for(ChunkCoordinates coord : chunks){
			if(coord.isSameChunk(e.getChunk())){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void afterPlay(HCAfterPlayEvent e){
		getApi().unregisterListener(this);
	}
	
	private class ChunkCoordinates{
		int x;
		int z;
		
		public ChunkCoordinates(Chunk chunk){
			this.x = chunk.getX();
			this.z = chunk.getZ();
		}
		
		public boolean isSameChunk(Chunk chunk){
			return (this.x == chunk.getX() && this.z == chunk.getZ());
		}
		
		@Override
		public String toString(){
			return "X:"+x+" Z:"+z;
		}
	}
}
