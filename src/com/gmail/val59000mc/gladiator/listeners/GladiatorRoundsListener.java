package com.gmail.val59000mc.gladiator.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.gladiator.callbacks.GladiatorCallbacks;
import com.gmail.val59000mc.gladiator.common.Constants;
import com.gmail.val59000mc.gladiator.events.GladiatorEndRoundEvent;
import com.gmail.val59000mc.gladiator.events.GladiatorStartRoundEvent;
import com.gmail.val59000mc.gladiator.players.GladiatorPlayer;
import com.gmail.val59000mc.gladiator.players.GladiatorTeam;
import com.gmail.val59000mc.gladiator.rounds.Round;
import com.gmail.val59000mc.gladiator.rounds.RoundManager;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterPlayEvent;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerEliminatedEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.Time;
import com.google.common.collect.Sets;

public class GladiatorRoundsListener extends HCListener{
	
	private RoundManager rounds;
	private List<HCPlayer> lastGladiators;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		rounds = ((GladiatorCallbacks) getCallbacksApi()).getRounds();
		lastGladiators = new ArrayList<HCPlayer>();
	}
	
	@EventHandler
	public void afterStartAllPlayers(HCAfterPlayEvent e){
		findGladiatorAndStartRound();
	}
	
	@EventHandler
	public void afterEndRound(GladiatorEndRoundEvent e){
		
		// messages and rewards
		if(e.getRound().getWinner() == null){
			getStringsApi()
				.get("gladiator.round-ended")
				.replace("%number%", String.valueOf(rounds.getRoundNumber()))
				.sendTitle(10, 50, 10)
				.sendChatP();
		}else{
			GladiatorTeam winner = e.getRound().getWinner();
			
			getStringsApi()
			.get("gladiator.round-ended-winner")
			.replace("%number%", String.valueOf(rounds.getRoundNumber()))
			.replace("%team%", winner.getColoredName())
			.sendTitle(10, 50, 10);
			
			getStringsApi()
				.get("gladiator.round-ended-winner-chat")
				.replace("%number%", String.valueOf(rounds.getRoundNumber()))
				.replace("%team%", winner.getColoredName())
				.sendChatP();

			GladiatorTeam slaveTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.SLAVES_TEAM_NAME);
			GladiatorTeam gladiatorTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.GLADIATOR_TEAM_NAME);
			
			if(winner.isGladiatorTeam()){
				GladiatorPlayer gladiator = e.getRound().getGladiator();
				gladiator.addRoundWonAsGladiator();
				getPmApi().rewardMoneyTo(gladiator, getApi().getConfig().getDouble("rewards.win-round-gladiator",Constants.REWARD_WIN_ROUND_AS_GLADIATOR));
				
				for(HCPlayer hcPlayer : getPmApi().getPlayers()){
					((PlayersManager) getPmApi()).spectatePlayer(hcPlayer);
				}
				
				getSoundApi().play(gladiatorTeam, Sound.LEVEL_UP, 1, 1);
				getSoundApi().play(slaveTeam, Sound.WITHER_HURT, 1, 2);
			}else if(winner.isSlaveTeam()){
				for(HCPlayer hcPlayer : winner.getMembers(true,null)){
					GladiatorPlayer slave = (GladiatorPlayer) hcPlayer;
					slave.addRoundWonAsSlave();
					getPmApi().rewardMoneyTo(slave, getApi().getConfig().getDouble("rewards.win-round-slave",Constants.REWARD_WIN_ROUND_AS_SLAVE));
				}

				for(HCPlayer hcPlayer : getPmApi().getPlayers()){
					((PlayersManager) getPmApi()).spectatePlayer(hcPlayer);
				}
				
				getSoundApi().play(slaveTeam, Sound.LEVEL_UP, 1, 1);
				getSoundApi().play(gladiatorTeam, Sound.WITHER_HURT, 1, 2);
			}
		}

		
		clearGroundItemsAndMobs();

		getPmApi().updatePlayersScoreboards();	
		
		// check if start new round
		if(!checkIfStartNewRound()){
			getApi().endGame(null);
			getApi().sync(()->getStringsApi().get("gladiator.end.title").sendTitle(20, 40, 20), 70);
			getSoundApi().play(Sound.ENDERDRAGON_GROWL, 1, 2);
			return;
		}
		
		getApi().buildTask("wait for new round", new HCTask() {
			
			private int remainingTime = 10;
			@Override
			public void run() {

				getStringsApi()
					.get("gladiator.next-round-in")
					.replace("%time%", String.valueOf(remainingTime))
					.sendActionBar();
				if(remainingTime < 5){
					getSoundApi().play(Sound.NOTE_STICKS, 1, 2);
				}
				remainingTime--;
				
			}
		})
		.withIterations(10)
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {

				if(checkIfStartNewRound()){
					randomizeTeamsForNextRound();
					findGladiatorAndStartRound();
				}else{
					getApi().endGame(null);
					getStringsApi().get("gladiator.end.title").sendTitle(20, 40, 20);
					getSoundApi().play(Sound.ENDERDRAGON_GROWL, 1, 2);
				}
				
			}
			
		}).addListener(new HCTaskListener(){
			
			@EventHandler
			public void onGameEnds(HCBeforeEndEvent e){
				getScheduler().stop();
			}
			
		})
		.build()
		.start();
	}
	
	private void clearGroundItemsAndMobs() {
		Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable() {
			public void run() {

				for(Entity entity : getApi().getWorldConfig().getWorld().getEntities()){
					switch(entity.getType()){
						case ARMOR_STAND:
						case PLAYER:
							// don't remove
							break;
						default:
							entity.remove();
							break;
					}
				}
			}
		}, 10);
		
	}

	private boolean checkIfStartNewRound() {
		if(getApi().is(GameState.PLAYING) && !rounds.getCurrentRound().isRunning()){
			
			if(getPmApi().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.PLAYING, PlayerState.DEAD)).size() <= 1){
				return false;
			}else if(rounds.getRoundNumber() < 3){
				return true;
			}
		}
		
		return false;
	}

	private void randomizeTeamsForNextRound(){
		for(HCPlayer hcPlayer : getPmApi().getPlayers()){
			getPmApi().removePlayerFromTeamSilently(hcPlayer);
		}
		
		List<HCPlayer> deadPlayers = getPmApi().getPlayers(true, PlayerState.DEAD);
		
		// get new random gladiator different than the previous ones (10 tries)
		HCPlayer newGladiator = null;
		int randomFailures = 0;
		while(randomFailures < 10){
			newGladiator = deadPlayers.get(Randoms.randomInteger(0, deadPlayers.size()-1));
			if(!lastGladiators.contains(newGladiator)){
				break;
			}
			randomFailures++;
		}
		GladiatorTeam slaveTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.SLAVES_TEAM_NAME);
		GladiatorTeam gladiatorTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.GLADIATOR_TEAM_NAME);
		
		getPmApi().addPlayerToTeam(newGladiator, gladiatorTeam);
		
		for(HCPlayer hcPlayer : deadPlayers){
			if(!hcPlayer.hasTeam()){
				getPmApi().addPlayerToTeam(hcPlayer, slaveTeam);
			}
			hcPlayer.setSpawnPoint(getCallbacksApi().getNextRespawnLocation(hcPlayer));
			getPmApi().revivePlayer(hcPlayer);
		}
	}

	private void findGladiatorAndStartRound(){
		
		GladiatorPlayer currentGladiator = null;
		
		for(HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)){
			GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
			if(gladiatorPlayer.isGladiator()){
				currentGladiator = gladiatorPlayer;
				break;
			}
		}
		
		if(currentGladiator != null){
			Round round = new Round(getApi(), currentGladiator);
			rounds.startRound(round);
		}else{
			getApi().endGame(null);
		}
	}
	
	@EventHandler
	public void onNewRound(GladiatorStartRoundEvent e){
		
		lastGladiators.add(e.getRound().getGladiator());
		
		// title for spec
		getStringsApi()
			.get("gladiator.rounds.spec."+String.valueOf(rounds.getRoundNumber()))
			.replace("%player%", e.getRound().getGladiator().getName())
			.sendTitle(getPmApi().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.SPECTATING, PlayerState.VANISHED)),10, 60, 10);
		
		GladiatorTeam slaveTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.SLAVES_TEAM_NAME);
		GladiatorTeam gladiatorTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.GLADIATOR_TEAM_NAME);

		// title for gladiator
		getStringsApi()
			.get("gladiator.rounds.gladiator."+String.valueOf(rounds.getRoundNumber()))
			.replace("%player%", e.getRound().getGladiator().getName())
			.sendTitle(gladiatorTeam, 10, 60, 10);

		// title for slaves
		getStringsApi()
			.get("gladiator.rounds.slaves."+String.valueOf(rounds.getRoundNumber()))
			.replace("%player%", e.getRound().getGladiator().getName())
			.sendTitle(slaveTeam, 10, 60, 10);
		
		// message for chat
		getStringsApi()
			.get("gladiator.rounds.chat."+String.valueOf(rounds.getRoundNumber()))
			.replace("%player%", e.getRound().getGladiator().getName())
			.sendChatP();

		String remainingTime = Time.getFormattedTime(e.getRound().getRemainingTime());
		
		for(HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)){
			if(hcPlayer.hasTeam()){
				GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
				if(gladiatorPlayer.isGladiator()){
					getStringsApi()
						.get("gladiator.start-action-bar.gladiator")
						.replace("%time%", remainingTime)
						.sendActionBar(hcPlayer, 8);
				}else{
					getStringsApi()
						.get("gladiator.start-action-bar.slave")
						.replace("%time%", remainingTime)
						.sendActionBar(hcPlayer, 8);
				}
			}
		}
		
		getSoundApi()
			.play(Sound.ENDERDRAGON_GROWL, 1, 2);

	}

	@EventHandler
	public void onPlayerEliminated(HCPlayerEliminatedEvent e){
		checkEndOfRoundAfterElimination((GladiatorPlayer) e.getHcPlayer());
	}
	
	private void checkEndOfRoundAfterElimination(GladiatorPlayer hcEliminated){
		
		Round round = rounds.getCurrentRound();
		
		if(rounds.getCurrentRound() != null && rounds.getCurrentRound().isRunning()){

			GladiatorTeam slaveTeam = (GladiatorTeam) getApi().getPlayersManagerAPI().getHCTeam(Constants.SLAVES_TEAM_NAME);
			
			if(hcEliminated.isGladiator() && hcEliminated.equals(rounds.getCurrentRound().getGladiator())){
				round.setWinner(slaveTeam);
				Bukkit.getPluginManager().callEvent(new GladiatorEndRoundEvent(getApi(), round));
			}else{
				if(slaveTeam.getMembers(true,PlayerState.PLAYING).size() == 0){
					round.setWinner((GladiatorTeam) round.getGladiator().getTeam());
					Bukkit.getPluginManager().callEvent(new GladiatorEndRoundEvent(getApi(), round));
				}
			}
			
		}else if(getPmApi().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.PLAYING, PlayerState.DEAD)).size() < 2){
			getApi().endGame(null);
		}
	}

}
