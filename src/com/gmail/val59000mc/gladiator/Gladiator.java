package com.gmail.val59000mc.gladiator;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.gladiator.callbacks.GladiatorCallbacks;
import com.gmail.val59000mc.gladiator.listeners.GladiatorBlockListener;
import com.gmail.val59000mc.gladiator.listeners.GladiatorChunkListener;
import com.gmail.val59000mc.gladiator.listeners.GladiatorMySQLListener;
import com.gmail.val59000mc.gladiator.listeners.GladiatorPlayerQuitListener;
import com.gmail.val59000mc.gladiator.listeners.GladiatorRoundsListener;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;

public class Gladiator extends JavaPlugin{
		
	public void onEnable(){
		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(),"config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(),"lang.yml"));
		
		HCGameAPI game = new HCGame.Builder("Gladiator", this, config, lang)
				.withPluginCallbacks(new GladiatorCallbacks())
				.withDefaultListenersAnd(Sets.newHashSet(
					new GladiatorPlayerQuitListener(),
					new GladiatorRoundsListener(),
					new GladiatorBlockListener(),
					new GladiatorChunkListener(),
					new GladiatorMySQLListener()
				))
				.build();
		game.loadGame();
	}
	
	public void onDisable(){
	}
}
