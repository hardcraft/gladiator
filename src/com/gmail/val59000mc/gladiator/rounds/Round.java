package com.gmail.val59000mc.gladiator.rounds;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.gladiator.common.Constants;
import com.gmail.val59000mc.gladiator.events.GladiatorEndRoundEvent;
import com.gmail.val59000mc.gladiator.players.GladiatorPlayer;
import com.gmail.val59000mc.gladiator.players.GladiatorTeam;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Time;

public class Round {

	private HCGameAPI api;
	private int remainingTime;
	private GladiatorPlayer gladiator;
	private GladiatorTeam winner;
	private boolean isRunning;
	
	public Round(HCGameAPI api, GladiatorPlayer gladiator) {
		super();
		this.api = api;
		this.remainingTime = api.getConfig().getInt("round-duration-seconds",Constants.ROUND_DURATION);
		this.gladiator = gladiator;
		this.winner = null;
		this.isRunning = false;
	}
	
	public int getRemainingTime() {
		return remainingTime;
	}
	
	public GladiatorPlayer getGladiator() {
		return gladiator;
	}
	
	public void setWinner(GladiatorTeam winner){
		this.winner = winner;
	}
	
	public void start(){
		this.isRunning = true;
		api.buildTask("round duration", new HCTask() {
			
			@Override
			public void run() {
				if(remainingTime > 0){
					api.getPlayersManagerAPI().updatePlayersScoreboards();	
					if(remainingTime <= 5 || remainingTime == 30 || remainingTime == 15){
						api.getStringsAPI()
							.get("gladiator.end-of-round-in")
							.replace("%time%", Time.getFormattedTime(remainingTime))
							.sendChatP();
						api.getSoundAPI().play(Sound.NOTE_SNARE_DRUM, 0.5f, 2);
					}
					remainingTime--;
				}else{
					setWinner((GladiatorTeam) api.getPlayersManagerAPI().getHCTeam(Constants.GLADIATOR_TEAM_NAME));
					isRunning = false;
					Bukkit.getPluginManager().callEvent(new GladiatorEndRoundEvent(api, Round.this));
				}
			}
		})
		.addListener(new HCTaskListener(){
			
			@EventHandler(priority=EventPriority.LOWEST)
			public void onEndRound(GladiatorEndRoundEvent e){
				isRunning = false;
				getScheduler().stop();
			}
			
		})
		.withInterval(20)
		.build()
		.start();
	}
	
	public boolean isRunning(){
		return isRunning;
	}

	public GladiatorTeam getWinner() {
		return winner;
	}
	
	
	
}
