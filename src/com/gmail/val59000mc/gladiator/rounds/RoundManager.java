package com.gmail.val59000mc.gladiator.rounds;

import com.gmail.val59000mc.gladiator.events.GladiatorStartRoundEvent;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;

public class RoundManager {

	private int roundNumber;
	private HCGameAPI api;
	private Round currentRound;
	
	public RoundManager(HCGameAPI api) {
		super();
		this.api = api;
		this.currentRound = null;
		this.roundNumber = 0;
	}
	public int getRoundNumber() {
		return roundNumber;
	}
	public Round getCurrentRound() {
		return currentRound;
	}
	public void startRound(Round round){
		roundNumber++;
		this.currentRound = round;

		api.getPlugin().getServer().getPluginManager().callEvent(new GladiatorStartRoundEvent(api,currentRound));
		this.currentRound.start();
	}
	public long getRemainingTime() {
		if(currentRound != null){
			return currentRound.getRemainingTime();
		}else{
			return 0;
		}
	}
	
	
	
	
}
