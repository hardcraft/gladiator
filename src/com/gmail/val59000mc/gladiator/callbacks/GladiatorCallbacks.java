package com.gmail.val59000mc.gladiator.callbacks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.gmail.val59000mc.gladiator.common.Constants;
import com.gmail.val59000mc.gladiator.common.GladiatorItems;
import com.gmail.val59000mc.gladiator.events.GladiatorEndRoundEvent;
import com.gmail.val59000mc.gladiator.events.GladiatorStartRoundEvent;
import com.gmail.val59000mc.gladiator.players.GladiatorPlayer;
import com.gmail.val59000mc.gladiator.players.GladiatorTeam;
import com.gmail.val59000mc.gladiator.rounds.RoundManager;
import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.spigotutils.Time;

public class GladiatorCallbacks extends DefaultPluginCallbacks{
	
	
	/////////////////////
	// custom
	//////////////////


	private GladiatorItems items;
	private RoundManager rounds;
	
	private void endRound(GladiatorPlayer gladiatorPlayer) {
		// TODO Auto-generated method stub
		
	}
	
	public GladiatorItems getItems() {
		return items;
	}

	public RoundManager getRounds() {
		return rounds;
	}
	
	
	
	//////////////////
	// callbacks
	///////////////////


	@Override
	public HCPlayer newHCPlayer(Player player){
		return new GladiatorPlayer(player);
	}
	
	@Override
	public HCPlayer newHCPlayer(HCPlayer hcPlayer){
		return new GladiatorPlayer(hcPlayer);
	}

	
	@Override
	public List<HCTeam> createTeams() {
		List<HCTeam> teams = new ArrayList<HCTeam>();
		teams.add(new GladiatorTeam(Constants.GLADIATOR_TEAM_NAME, ChatColor.RED, parseLocations(getConfig().getStringList("teams.gladiator"))));
		teams.add(new GladiatorTeam(Constants.SLAVES_TEAM_NAME, ChatColor.BLUE, parseLocations(getConfig().getStringList("teams.slaves"))));
		return teams;
	}
	
	private List<Location> parseLocations(List<String> locationsStr){
		List<Location> locations = new ArrayList<Location>();
		if(locationsStr == null){
			return locations;
		}
		World world = getApi().getWorldConfig().getWorld();
		for(String locationStr : locationsStr){
			locations.add(Parser.parseLocation(world, locationStr));
		}
		return locations;
	}
	

	@Override
	public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {
		
		List<String> content = new ArrayList<String>();
		
		if(rounds.getCurrentRound() != null){
			if(rounds.getCurrentRound().isRunning()){

				// current round
				content.add(getStringsApi().get("gladiator.scoreboard.round").toString());
				content.add(" "+ChatColor.GREEN+rounds.getRoundNumber());
				
				// remaining time
				content.add(getStringsApi().get("gladiator.scoreboard.remaining-time").toString());
				content.add(" "+ChatColor.GREEN+Time.getFormattedTime(rounds.getRemainingTime()));
				
				// remaining living slaves
				content.add(getStringsApi().get("gladiator.scoreboard.living-slaves").toString());
				content.add(" "+ChatColor.GREEN+getPmApi().getHCTeam(Constants.SLAVES_TEAM_NAME).getMembers(true, PlayerState.PLAYING).size());
				
			}else{
				
				// last ended round
				content.add(getStringsApi().get("gladiator.scoreboard.last-ended-round").toString());
				content.add(" "+ChatColor.GREEN+rounds.getRoundNumber());
				
			}
		}

		content.add(" ");
		
		// Kills / Deaths
		content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
		content.add(" "+ChatColor.GREEN+""+hcPlayer.getKills()+ChatColor.WHITE+"/"+ChatColor.GREEN+hcPlayer.getDeaths());
		
		// Coins
		content.add(getStringsApi().get("messages.scoreboard.coins").toString());
		content.add(" "+ChatColor.GREEN+""+hcPlayer.getMoney());

		content.add(" ");
		
		// Team
		if(rounds.getCurrentRound() != null && rounds.getCurrentRound().isRunning()){
			content.add(getStringsApi().get("gladiator.scoreboard.gladiator").toString());
			content.add(" "+rounds.getCurrentRound().getGladiator().getColoredName());
		}							
		
		return content;
	}
	
	@Override
	public HCTeam getAssignableRandomTeam() {
		HCTeam gladiatorTeam = getPmApi().getHCTeam(Constants.GLADIATOR_TEAM_NAME);
		if(gladiatorTeam.getMembers(null, null).size() == 0){
			return gladiatorTeam;
		}else{
			return getPmApi().getHCTeam(Constants.SLAVES_TEAM_NAME);
		}
	}

	@Override
	public Location getFirstRespawnLocation(HCPlayer hcPlayer){
		return getNextRespawnLocation(hcPlayer);
	}

	@Override
	public Location getNextRespawnLocation(HCPlayer hcPlayer) {
		if(hcPlayer.hasTeam()){
			return ((GladiatorTeam) hcPlayer.getTeam()).getRandomSpawnpoint();
		}
		return hcPlayer.getSpawnPoint();
	}
	
	@Override
	public void startGameMessages() {
		// nothing
	}

	@Override
	public void startPlayer(HCPlayer hcPlayer) {

		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
		
		if(hcPlayer.isOnline()){
			
			Player player = hcPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			player.setFireTicks(0);
			
			if(gladiatorPlayer.isGladiator()){
				int health = gladiatorPlayer.getGladiatorHalfHearts();
				player.setMaxHealth(health);
				player.setHealth(health);
			}else{
				player.setMaxHealth(20);
				player.setHealth(20);
			}
			
			player.setFoodLevel(20);
			player.setSaturation(20);
			Effects.clear(player);
			Inventories.clear(player);
			
			getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);
			
			if(hcPlayer.getSpawnPoint() == null)
				hcPlayer.getPlayer().teleport(getApi().getWorldConfig().getCenter());
			else
				hcPlayer.getPlayer().teleport(hcPlayer.getSpawnPoint());
			
		}
	}
	
	@Override
	public void respawnPlayer(HCPlayer hcPlayer) {
		
		// should not happen
		
	}
	
	@Override
	public void revivePlayer(HCPlayer hcPlayer) {
		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
		
		if(hcPlayer.isOnline()){

			
			if(!gladiatorPlayer.isGladiator() && rounds.getCurrentRound() != null && rounds.getCurrentRound().isRunning()){

				getStringsApi()
					.get("gladiator.respawn-slave")
					.sendTitle(hcPlayer, 20, 20, 20);
				
				getStringsApi()
					.get("gladiator.slave-has-respawned")
					.replace("%player%", hcPlayer.getName())
					.sendChatP();

				getSoundApi().play(Sound.ANVIL_USE, 0.5f, 2f);
				
			}
			
			Player player = hcPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			player.setFireTicks(0);
			
			if(gladiatorPlayer.isGladiator()){
				int health = gladiatorPlayer.getGladiatorHalfHearts();
				player.setMaxHealth(health);
				player.setHealth(health);
			}else{
				player.setMaxHealth(20);
				player.setHealth(20);
			}
			
			player.setFoodLevel(20);
			player.setSaturation(20);
			Effects.clear(player);
			Inventories.clear(player);
			getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);

			
			if(hcPlayer.getSpawnPoint() == null){
				hcPlayer.getPlayer().teleport(getApi().getWorldConfig().getCenter());
			}else{
				clearSpawnAreaBlocks(hcPlayer.getSpawnPoint());
				hcPlayer.getPlayer().teleport(hcPlayer.getSpawnPoint());
			}
			
		}
		
	}

	
	private void clearSpawnAreaBlocks(Location ref) {
		for(int x=-3 ; x<=3 ; x++){
			for(int z=-3 ; z<=3 ; z++){
				for(int y=0 ; y<=3 ; y++){
					ref.clone().add(x, y, z).getBlock().setType(Material.AIR);
				}
			}
		}
	}

	@Override
	public void eliminatedPlayer(HCPlayer hcPlayer){

		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
		
		if(!gladiatorPlayer.isGladiator()){

			getApi().buildTask("respawn "+hcPlayer.getName(), new HCTask() {

				private int remainingTime = getConfig().getInt("slave-respawn-time",Constants.SLAVE_RESPAWN_TIME);
				
				@Override
				public void run() {
					
					if(hcPlayer.isOnline()){

						if(remainingTime > 0){
							getStringsApi()
								.get("gladiator.respawn-slave-in")
								.replace("%time%", String.valueOf(remainingTime))
								.sendTitle(hcPlayer, 0, 21, 0);
							if(remainingTime < 5){
								getSoundApi().play(hcPlayer, Sound.NOTE_STICKS, 1, 2);
							}
							remainingTime--;
						}else{
							getPmApi().revivePlayer(hcPlayer);
							scheduler.stop();
						}
						
					}
				}
			})
			.withDelay(1)
			.withInterval(20)
			.withIterations(-1)
			.addListener(new HCTaskListener(){
				
				@EventHandler
				public void onEndRound(GladiatorEndRoundEvent e){
					getScheduler().stop();
				}
				
				@EventHandler
				public void onNewRound(GladiatorStartRoundEvent e){
					getScheduler().stop();
				}
				
				@EventHandler
				public void onGameEnds(HCBeforeEndEvent e){
					getScheduler().stop();
				}
				
			})
			.build()
			.start();
			
		}
		
	}

	@Override
	public void assignStuffToPlayer(HCPlayer hcPlayer){
		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
		if(gladiatorPlayer.isGladiator()){
			hcPlayer.setStuff(new Stuff(hcPlayer.getName(), items.getGladiatorItems(gladiatorPlayer), items.getGladiatorArmor(gladiatorPlayer)));
		}else{
			hcPlayer.setStuff(new Stuff(hcPlayer.getName(), items.getSlaveItems(gladiatorPlayer), items.getSlaveArmor(gladiatorPlayer)));
		}
	}
	
	@Override
	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {

		GladiatorPlayer gKilled = (GladiatorPlayer) hcKilled;
		GladiatorPlayer gKiller = (GladiatorPlayer) hcKiller;
		
		if(gKilled.isGladiator() && gKiller.isSlave()){
			gKiller.addGladiatorKilled();
			getPmApi().rewardMoneyTo(gKiller, getConfig().getDouble("rewards.kill-gladiator",Constants.REWARD_KILL_GLADIATOR));
		}else if(gKilled.isSlave() && gKiller.isGladiator()){
			gKiller.addSlavesKilled();
			if(gKiller.isOnline()){
				Player gladiator = gKiller.getPlayer();
				if(!gladiator.isDead()){
					double setHealth = (gladiator.getMaxHealth()-gladiator.getHealth()) > 2 ? gladiator.getHealth()+2 : gladiator.getMaxHealth();
					gladiator.setHealth(setHealth);
				}
			}
			getPmApi().rewardMoneyTo(gKiller, getConfig().getDouble("rewards.kill-slave",Constants.REWARD_KILL_SLAVE));
		}
		
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
		
	}
	
	@Override
	public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

		if(hcPlayer.isOnline()){

			GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
			
			Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("gladiator.end.summary")
					.replace("%game%", getApi().getName())
					.replace("%kills%", String.valueOf(hcPlayer.getKills()))
					.replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
					.replace("%gladiatorkilled%", String.valueOf(gladiatorPlayer.getGladiatorKilled()))
					.replace("%slaveskilled%", String.valueOf(gladiatorPlayer.getSlavesKilled()))
					.replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
					.replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
					.replace("%roundswongladiator%", String.valueOf(gladiatorPlayer.getRoundsWonAsGladiator()))
					.replace("%roundswonslave%", String.valueOf(gladiatorPlayer.getRoundsWonAsSlave()))
					.toString()
			);
				
		}
		
	}
	
	@Override
	public void endGameMessages(HCTeam winningTeam) {
		
		// no message
	}
}
