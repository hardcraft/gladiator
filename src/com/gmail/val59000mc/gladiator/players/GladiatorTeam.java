package com.gmail.val59000mc.gladiator.players;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.gmail.val59000mc.gladiator.common.Constants;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Randoms;

public class GladiatorTeam extends HCTeam {

	private List<Location> spawnpoints;
	
	public GladiatorTeam(String name, ChatColor color, List<Location> spawnpoints) {
		super(name, color, null);
		this.spawnpoints = spawnpoints;
	}

	public boolean isGladiatorTeam() {
		return getName().equals(Constants.GLADIATOR_TEAM_NAME);
	}

	public boolean isSlaveTeam() {
		return getName().equals(Constants.SLAVES_TEAM_NAME);
	}
	
	public Location getRandomSpawnpoint(){
		return spawnpoints.get(Randoms.randomInteger(0, spawnpoints.size()-1)).clone();
	}
	
	public List<Location> getSpawnpoints(){
		return this.spawnpoints;
	}

}
