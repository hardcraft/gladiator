package com.gmail.val59000mc.gladiator.players;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class GladiatorPlayer extends HCPlayer{
	
	// stats
	private int gladiatorKilled;
	private int slavesKilled;
	private int roundsWonAsGladiator;
	private int roundsWonAsSlave;
	
	// perks
	private int gladiatorHalfHearts;
	private int slaveBowChance;
	private int slaveExtraChance;
	private int slavePotionChance;
	private int enchantChance;
	
	
	public GladiatorPlayer(Player player) {
		super(player);
		
		this.roundsWonAsGladiator = 0;
		this.roundsWonAsSlave = 0;
		this.gladiatorKilled = 0;
		this.slavesKilled = 0;
		
		this.gladiatorHalfHearts = 40;
		this.slaveBowChance = 20;
		this.slaveExtraChance = 20;
		this.slavePotionChance = 20;
		this.enchantChance = 20;
	}
	
	public GladiatorPlayer(HCPlayer hcPlayer){
		super(hcPlayer);
		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) hcPlayer;
		
		this.roundsWonAsGladiator = gladiatorPlayer.roundsWonAsGladiator;
		this.roundsWonAsSlave = gladiatorPlayer.roundsWonAsSlave;
		this.gladiatorKilled = gladiatorPlayer.gladiatorKilled;
		this.slavesKilled = gladiatorPlayer.slavesKilled;
		
		this.gladiatorHalfHearts = gladiatorPlayer.gladiatorHalfHearts;
		this.slaveBowChance = gladiatorPlayer.slaveBowChance;
		this.slaveExtraChance = gladiatorPlayer.slaveExtraChance;
		this.slavePotionChance = gladiatorPlayer.slavePotionChance;
		this.enchantChance = gladiatorPlayer.enchantChance;
	}
	
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
		GladiatorPlayer gladiatorPlayer = (GladiatorPlayer) lastUpdatedSession;
		this.roundsWonAsGladiator -= gladiatorPlayer.roundsWonAsGladiator;
		this.roundsWonAsSlave -= gladiatorPlayer.roundsWonAsSlave;
		this.gladiatorKilled -= gladiatorPlayer.gladiatorKilled;
		this.slavesKilled -= gladiatorPlayer.slavesKilled;
	}

	public boolean isGladiator() {
		return hasTeam() && ((GladiatorTeam) getTeam()).isGladiatorTeam();
	}

	public boolean isSlave() {
		return hasTeam() && ((GladiatorTeam) getTeam()).isSlaveTeam();
	}

	public int getGladiatorHalfHearts() {
		return gladiatorHalfHearts;
	}

	public void setGladiatorHalfHearts(int gladiatorHalfHearts) {
		this.gladiatorHalfHearts = gladiatorHalfHearts;
	}

	public int getSlaveBowChance() {
		return slaveBowChance;
	}

	public void setSlaveBowChance(int slaveBowChance) {
		this.slaveBowChance = slaveBowChance;
	}

	public int getSlaveExtraChance() {
		return slaveExtraChance;
	}

	public void setSlaveExtraChance(int slaveExtraChance) {
		this.slaveExtraChance = slaveExtraChance;
	}

	public int getSlavePotionChance() {
		return slavePotionChance;
	}

	public void setSlavePotionChance(int slavePotionChance) {
		this.slavePotionChance = slavePotionChance;
	}

	public int getEnchantChance() {
		return enchantChance;
	}

	public void setEnchantChance(int enchantChance) {
		this.enchantChance = enchantChance;
	}

	public int getGladiatorKilled() {
		return gladiatorKilled;
	}

	public void addGladiatorKilled(){
		this.gladiatorKilled++;
	}

	public int getSlavesKilled() {
		return slavesKilled;
	}

	public void addSlavesKilled(){
		this.slavesKilled++;
	}

	public int getRoundsWonAsGladiator() {
		return roundsWonAsGladiator;
	}

	public void addRoundWonAsGladiator(){
		this.roundsWonAsGladiator++;
		this.addWin();
	}

	public int getRoundsWonAsSlave() {
		return roundsWonAsSlave;
	}

	public void addRoundWonAsSlave(){
		this.roundsWonAsSlave++;
		this.addWin();
	}
	
	
}
