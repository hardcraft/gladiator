//package com.gmail.val59000mc.gladiator.old.threads;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import org.bukkit.Bukkit;
//import org.bukkit.ChatColor;
//import org.bukkit.entity.Player;
//import org.bukkit.scoreboard.DisplaySlot;
//import org.bukkit.scoreboard.Objective;
//import org.bukkit.scoreboard.Scoreboard;
//
//import com.gmail.val59000mc.gladiator.Gladiator;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.gladiator.old.players.PlayerState;
//import com.gmail.val59000mc.gladiator.old.players.PlayersManager;
//import com.gmail.val59000mc.simpleinventorygui.spigotutils.Numbers;
//import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
//
//public class UpdateScoreboardThread implements Runnable{
//	
//
//	private static UpdateScoreboardThread instance;
//	private Map<GPlayer,SimpleScoreboard> scoreboards;
//	
//
//
//	public static void start(){
//		if(instance == null){
//			instance = new UpdateScoreboardThread();
//		}
//		Bukkit.getScheduler().runTaskAsynchronously(Gladiator.getPlugin(), instance);
//	}
//	
//	public static void add(GPlayer gPlayer){
//		if(instance == null){
//			instance = new UpdateScoreboardThread();
//		}
//		instance.addGPlayer(gPlayer);
//	}
//	
//	private void addGPlayer(GPlayer gPlayer){
//		if(!scoreboards.containsKey(gPlayer)){
//			SimpleScoreboard sc = new SimpleScoreboard("Gladiator");
//			if(gPlayer.isOnline()){
//				setupTeamColors(sc,gPlayer);
//			}
//			getScoreboards().put(gPlayer, sc);
//		}
//	}
//	
//	private void setupTeamColors(SimpleScoreboard sc, GPlayer gPlayer) {
//		PlayersManager pm = PlayersManager.instance();
//		
//		Scoreboard scoreboard = sc.getBukkitScoreboard();
//		Objective life = scoreboard.registerNewObjective(ChatColor.DARK_RED+"\u2764", "health");
//		life.setDisplaySlot(DisplaySlot.BELOW_NAME);
//		
//		
//		// Putting players in colored teams
//		for(GPlayer aGPlayer : pm.getPlayers()){
//				
//			if(aGPlayer.isOnline()){
//
//				life.getScore(aGPlayer.getName()).setScore((int) Math.round(aGPlayer.getPlayer().getHealth()));
//			}
//		}
//	}
//	
//	private UpdateScoreboardThread(){
//		this.scoreboards = Collections.synchronizedMap(new HashMap<GPlayer,SimpleScoreboard>());
//	}
//	
//	private synchronized Map<GPlayer,SimpleScoreboard> getScoreboards(){
//		return scoreboards;
//	}
//	
//	@Override
//	public void run() {
//		Bukkit.getScheduler().runTask(Gladiator.getPlugin(), new Runnable(){
//
//			@Override
//			public void run() {
//				
//				
//						
//					for(Entry<GPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
//						GPlayer gPlayer = entry.getKey();
//						SimpleScoreboard scoreboard = entry.getValue();
//						
//						if(gPlayer.isOnline() && gPlayer.getTeam() != null){
//
//							Player player = gPlayer.getPlayer();
//							
//							List<String> content = new ArrayList<String>();
//							content.add(" ");
//							
//							// Team
//							content.add(I18n.get("scoreboard.team",player));
//							content.add(" "+ChatColor.GREEN+I18n.get("team."+gPlayer.getTeam().getType(),player));
//							
//							// Kills / Deaths
//							content.add(I18n.get("scoreboard.kills",player));
//							content.add(" "+ChatColor.GREEN+""+gPlayer.getKills()+ChatColor.WHITE);
//							
//							// Coins
//							content.add(I18n.get("scoreboard.coins-earned",player));
//							content.add(" "+ChatColor.GREEN+""+Numbers.round(gPlayer.getMoney(),2));
//							
//							// Teammate
//							content.add(I18n.get("scoreboard.teammates",player));
//							for(GPlayer gTeamMate : gPlayer.getTeam().getMembers()){
//								if(gTeamMate.isState(PlayerState.PLAYING)){
//									content.add(" "+gPlayer.getColor()+""+gTeamMate.getName());
//								}else{
//									content.add(" "+ChatColor.GRAY+""+gTeamMate.getName());
//								}
//							}								
//							
//							scoreboard.clear();
//							for(String line : content){
//								scoreboard.add(line);
//							}
//							scoreboard.draw();
//							scoreboard.send(player);
//						}
//					}
//
//					Bukkit.getScheduler().runTaskLaterAsynchronously(Gladiator.getPlugin(), instance, 20);
//				}
//				
//			});
//				
//		
//	}
//	
//	
//	
//	
//}
