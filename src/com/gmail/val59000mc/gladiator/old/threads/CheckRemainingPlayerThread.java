//package com.gmail.val59000mc.gladiator.old.threads;
//
//import java.util.List;
//
//import org.bukkit.Bukkit;
//
//import com.gmail.val59000mc.gladiator.Gladiator;
//import com.gmail.val59000mc.gladiator.old.game.EndCause;
//import com.gmail.val59000mc.gladiator.old.game.GameManager;
//import com.gmail.val59000mc.gladiator.old.players.GTeam;
//import com.gmail.val59000mc.gladiator.old.players.PlayersManager;
//import com.gmail.val59000mc.spigotutils.Logger;
//
//
//public class CheckRemainingPlayerThread implements Runnable{
//
//	private static CheckRemainingPlayerThread instance;
//	
//	private boolean run;
//	
//	public static void start(){
//		Logger.debug("-> CheckRemainingPlayerThread::start");
//		CheckRemainingPlayerThread thread = new CheckRemainingPlayerThread();
//		Bukkit.getScheduler().runTaskAsynchronously(Gladiator.getPlugin(), thread);
//		Logger.debug("<- CheckRemainingPlayerThread::start");
//	}
//
//
//
//	public static void stop() {
//		instance.run = false;
//	}
//	
//	
//	public CheckRemainingPlayerThread(){
//		instance = this;
//		this.run = true;
//	}
//	
//	@Override
//	public void run() {
//		
//		Bukkit.getScheduler().runTask(Gladiator.getPlugin(), new Runnable(){
//
//			@Override
//			public void run(){
//				if(run){
//					List<GTeam> playingTeams = PlayersManager.instance().getPlayingTeams();
//					if(playingTeams.size() == 1){
//						GameManager.instance().endGame(EndCause.TEAM_WIN,playingTeams.get(0));
//					}
//					if(playingTeams.size() == 0){
//						GameManager.instance().endGame(EndCause.NO_MORE_PLAYERS,null);
//					}
//					
//					Bukkit.getScheduler().runTaskLaterAsynchronously(Gladiator.getPlugin(), instance, 20);
//				}
//			}
//			
//		});
//
//	}
//}
