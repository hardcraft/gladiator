//package com.gmail.val59000mc.gladiator.old.listeners;
//
//import org.bukkit.Bukkit;
//import org.bukkit.event.EventHandler;
//import org.bukkit.event.EventPriority;
//import org.bukkit.event.Listener;
//import org.bukkit.event.entity.PlayerDeathEvent;
//import org.bukkit.event.player.PlayerRespawnEvent;
//
//import com.gmail.val59000mc.gladiator.Gladiator;
//import com.gmail.val59000mc.gladiator.old.configuration.Config;
//import com.gmail.val59000mc.gladiator.old.dependencies.VaultManager;
//import com.gmail.val59000mc.gladiator.old.game.GameManager;
//import com.gmail.val59000mc.gladiator.old.game.GameState;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.gladiator.old.players.PlayerState;
//import com.gmail.val59000mc.gladiator.old.players.PlayersManager;
//import com.gmail.val59000mc.spigotutils.Logger;
//import com.gmail.val59000mc.spigotutils.Texts;
//
//public class PlayerDeathListener implements Listener {
//	
//	@EventHandler(priority = EventPriority.HIGHEST)
//	public void onPlayerDeath(final PlayerDeathEvent event) {
//
//		GPlayer wicPlayer = PlayersManager.instance().getGPlayer(event.getEntity());
//		
//		if(wicPlayer == null || !wicPlayer.isOnline()){
//			return;
//		}
//		
//		handleDeathPlayer(event,wicPlayer);
//		
//	}
//	
//	public void handleDeathPlayer(final PlayerDeathEvent event, GPlayer dead){
//		PlayersManager pm = PlayersManager.instance();
//		
//		// Add death score to dead player and kill score to killer if playing
//		if(GameManager.instance().isState(GameState.PLAYING)){
//			
//			GPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getGPlayer(dead.getPlayer().getKiller());
//			
//			if(killer != null && killer.isOnline()){
//				killer.addKill();
//				killer.addMoney(VaultManager.addMoney(killer.getPlayer(), Config.killReward));
//			}
//			
//			dead.setState(PlayerState.DEAD);
//			
//			if(dead.isOnline() && PlayersManager.instance().getPlayingTeams().size() > 1){
//
//				Texts.tellraw(dead.getPlayer(), I18n.get("stats.death",dead.getPlayer())
//					.replace("%kills%", String.valueOf(dead.getKills()))
//					.replace("%hardcoins%", String.valueOf(dead.getMoney()))
//				);
//				
//			}
//			
//			Bukkit.getScheduler().runTaskLater(Gladiator.getPlugin(), new Runnable() {
//				
//				public void run() {
//			        event.getEntity().spigot().respawn();
//				}
//			}, 20);
//		}
//		
//	}
//	
//	@EventHandler(priority = EventPriority.HIGHEST)
//	public void onPlayerRespawn(PlayerRespawnEvent event) {
//		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
//		
//		GPlayer wicPlayer = PlayersManager.instance().getGPlayer(event.getPlayer());
//		
//		if(wicPlayer == null){
//			return;
//		}
//		
//		handleRespawnPlayer(event,wicPlayer);
//		
//		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
//	}
//	
//	public void handleRespawnPlayer(PlayerRespawnEvent event, GPlayer wicPlayer){
//		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+wicPlayer);
//
//		final String name = wicPlayer.getName();
//		
//		switch(GameManager.instance().getState()){
//			case WAITING:
//					Bukkit.getScheduler().runTaskLater(Gladiator.getPlugin(), new Runnable() {
//					
//					@Override
//					public void run() {
//						Logger.debug("waitPlayer");
//						GPlayer p = PlayersManager.instance().getGPlayer(name);
//						if(p != null){
//							PlayersManager.instance().waitPlayer(p);
//						}
//						
//					}
//				}, 1);
//				break;
//			case LOADING:
//			case STARTING:
//			case ENDED:
//			default:
//			case PLAYING:
//				Bukkit.getScheduler().runTaskLater(Gladiator.getPlugin(), new Runnable() {
//					
//					@Override
//					public void run() {
//						Logger.debug("teamSpawnPlayer");
//						GPlayer p = PlayersManager.instance().getGPlayer(name);
//						if(p != null){
//							PlayersManager.instance().spectatePlayer(p);
//						}
//						
//					}
//				}, 1);
//				break;
//		}
//		
//
//		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
//	}
//	
//}
