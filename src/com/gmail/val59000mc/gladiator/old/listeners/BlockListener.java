package com.gmail.val59000mc.gladiator.old.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(final BlockBreakEvent event){
		event.setCancelled(true);
	}
}
