//package com.gmail.val59000mc.gladiator.old;
//
//import net.md_5.bungee.api.ChatColor;
//
//import org.bukkit.Bukkit;
//import org.bukkit.plugin.java.JavaPlugin;
//
//import com.gmail.val59000mc.gladiator.old.game.GameManager;
//import com.gmail.val59000mc.spigotutils.Logger;
//
//public class Gladiator extends JavaPlugin{
//	
//	private static Gladiator pl;
//	
//	
//	public void onEnable(){
//		pl = this;
//	
//		// Blocks players joins while loading the plugin
//		Bukkit.getServer().setWhitelist(true);
//		saveDefaultConfig();
//		
//		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"Gladiator"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
//		Logger.setStrippedPrefix("[Gladiator] ");
//		
//		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
//			
//			public void run() {
//				GameManager.instance().loadGame();
//				
//				// Unlock players joins and rely on UhcPlayerJoinListener
//				Bukkit.getServer().setWhitelist(false);
//			}
//			
//		}, 1);
//		
//		
//	}
//	
//	public static Gladiator getPlugin(){
//		return pl;
//	}
//	
//	public void onDisable(){
//	}
//}
