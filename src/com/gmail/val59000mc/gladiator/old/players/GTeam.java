//package com.gmail.val59000mc.gladiator.old.players;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.bukkit.ChatColor;
//import org.bukkit.Location;
//import org.bukkit.entity.Player;
//
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.spigotutils.Randoms;
//
//public class GTeam {
//
//	private List<GPlayer> members;
//	private TeamType type;
//	private List<Location> spawnPoints;
//	private Map<GPlayer,Location> assignedSpawnPoints;
//	private int limit;
//	
//	public GTeam(TeamType type, int limit, List<Location> locations) {
//		this.members = new ArrayList<GPlayer>();
//		this.type = type;
//		this.spawnPoints = locations;
//		this.assignedSpawnPoints = new HashMap<GPlayer,Location>();
//		this.limit = limit;
//	}
//	
//	public TeamType getType() {
//		return type;
//	}
//
//	public void setType(TeamType type) {
//		this.type = type;
//	}
//
//
//	public void sendI18nMessage(String code) {
//		for(GPlayer wicPlayer: members){
//			wicPlayer.sendI18nMessage(code);
//		}
//	}
//	
//	public String getI18nName(Player player){
//		return I18n.get("team."+getType().toString(), player);
//	}
//	
//	public boolean contains(GPlayer player){
//		return members.contains(player);
//	}
//	
//	public synchronized Map<GPlayer,Location> getAssignedSpawnPoints(){
//		return assignedSpawnPoints;
//	}
//
//	public synchronized List<Location> getFreeSpawnPoints(){
//		List<Location> freeLocations = spawnPoints;
//		for(Location loc : assignedSpawnPoints.values()){
//			freeLocations.remove(loc);
//		}
//		return freeLocations;
//	}
//	
//	public synchronized List<GPlayer> getMembers(){
//		return members;
//	}
//
//	public List<GPlayer> getPlayingMembers(){
//		List<GPlayer> playingMembers = new ArrayList<GPlayer>();
//		for(GPlayer uhcPlayer : getMembers()){
//			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
//				playingMembers.add(uhcPlayer);
//			}
//		}
//		return playingMembers;
//	}
//	
//	public synchronized List<String> getMembersNames(){
//		List<String> names = new ArrayList<String>();
//		for(GPlayer player : getMembers()){
//			names.add(player.getName());
//		}
//		return names;
//	}
//	
//	public void addPlayer(GPlayer gPlayer){
//		gPlayer.leaveTeam();
//		gPlayer.setTeam(this);
//		getMembers().add(gPlayer);
//		List<Location> freeLocations = getFreeSpawnPoints();
//		Location loc = freeLocations.get(Randoms.randomInteger(0, freeLocations.size()-1));
//		gPlayer.setSpawnPoint(loc);
//		assignedSpawnPoints.put(gPlayer,loc);
//	}
//
//	public boolean isFull(){
//		return getMembers().size() == limit;
//	}
//	
//	public void leave(GPlayer gPlayer){
//		getMembers().remove(gPlayer);
//		gPlayer.setSpawnPoint(null);
//		getAssignedSpawnPoints().remove(gPlayer);
//	}
//	
//	public boolean isOnline(){
//		for(GPlayer gPlayer : getMembers()){
//			if(gPlayer.isOnline()){
//				return true;
//			}
//		}
//		return false;
//	}
//
//
//	public boolean isPlaying() {
//		for(GPlayer gPlayer : getMembers()){
//			if(gPlayer.isOnline() && gPlayer.isState(PlayerState.PLAYING)){
//				return true;
//			}
//		}
//		return false;
//	}
//	
//	public List<GPlayer> getOtherMembers(GPlayer excludedPlayer){
//		List<GPlayer> otherMembers = new ArrayList<GPlayer>();
//		for(GPlayer uhcPlayer : getMembers()){
//			if(!uhcPlayer.equals(excludedPlayer))
//				otherMembers.add(uhcPlayer);
//		}
//		return otherMembers;
//	}
//
//	public Boolean is(TeamType type) {
//		return getType().equals(type);
//	}
//
//	public ChatColor getColor() {
//		return getType().getColor();
//	}
//
//}
