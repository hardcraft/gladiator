package com.gmail.val59000mc.gladiator.old.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
