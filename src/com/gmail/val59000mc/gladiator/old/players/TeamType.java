package com.gmail.val59000mc.gladiator.old.players;

import org.bukkit.ChatColor;

public enum TeamType {
	
	GLADIATOR(ChatColor.RED),
	SLAVES(ChatColor.GREEN);

	private ChatColor color;

	private TeamType(ChatColor color){
		this.color = color;
	}
	
	public ChatColor getColor() {
		return color;
	}
}
