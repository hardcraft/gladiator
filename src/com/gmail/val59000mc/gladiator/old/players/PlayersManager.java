//package com.gmail.val59000mc.gladiator.old.players;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//import org.bukkit.Bukkit;
//import org.bukkit.ChatColor;
//import org.bukkit.GameMode;
//import org.bukkit.Location;
//import org.bukkit.Sound;
//import org.bukkit.entity.Player;
//import org.bukkit.potion.PotionEffectType;
//
//import com.gmail.val59000mc.gladiator.old.classes.PlayerClassManager;
//import com.gmail.val59000mc.gladiator.old.configuration.Config;
//import com.gmail.val59000mc.gladiator.old.dependencies.VaultManager;
//import com.gmail.val59000mc.gladiator.old.game.EndCause;
//import com.gmail.val59000mc.gladiator.old.game.GameManager;
//import com.gmail.val59000mc.gladiator.old.game.GameState;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.gladiator.old.threads.UpdateScoreboardThread;
//import com.gmail.val59000mc.gladiator.old.titles.TitleManager;
//import com.gmail.val59000mc.simpleinventorygui.SIG;
//import com.gmail.val59000mc.spigotutils.Effects;
//import com.gmail.val59000mc.spigotutils.Inventories;
//import com.gmail.val59000mc.spigotutils.Locations;
//import com.gmail.val59000mc.spigotutils.Logger;
//import com.gmail.val59000mc.spigotutils.Sounds;
//import com.gmail.val59000mc.spigotutils.Texts;
//
//public class PlayersManager {
//	private static PlayersManager instance;
//	
//	private List<GPlayer> players;
//	private List<GTeam> teams;
//	
//	// static
//	
//	public static PlayersManager instance(){
//		if(instance == null){
//			instance = new PlayersManager();
//		}
//		
//		return instance;
//	}
//	
//	// constructor 
//	
//	private PlayersManager(){
//		players = Collections.synchronizedList(new ArrayList<GPlayer>());
//		teams = Collections.synchronizedList(new ArrayList<GTeam>());
//		
//
//		List<Location> gladiatorLocs = new ArrayList<Location>();
//		gladiatorLocs.add(Config.gladiatorLocation);
//		teams.add(new GTeam(TeamType.GLADIATOR,1,gladiatorLocs));
//
//		List<Location> locs = new ArrayList<Location>();
//		locs.addAll(Config.quarterOneLocation);
//		locs.addAll(Config.quarterTwoLocation);
//		locs.addAll(Config.quarterThreeLocation);
//		locs.addAll(Config.quarterFourLocation);
//		teams.add(new GTeam(TeamType.SLAVES, 20, locs));
//		
//	}
//	
//	
//	// Accessors
//	
//	public GPlayer getGPlayer(Player player){
//		return getGPlayer(player.getName());
//	}
//	
//	public GPlayer getGPlayer(String name){
//		for(GPlayer gPlayer : getPlayers()){
//			if(gPlayer.getName().equals(name))
//				return gPlayer;
//		}
//		
//		return null;
//	}
//	
//	public synchronized List<GPlayer> getPlayers(){
//		return players;
//	}
//	
//	public synchronized GTeam getTeam(TeamType type){
//		for(GTeam team : getTeams()){
//			if(team.getType().equals(type)){
//				return team;
//			}
//		}
//		return null;
//	}
//	
//	public synchronized List<GTeam> getTeams(){
//		return teams;
//	}
//
//	public List<GTeam> getPlayingTeams() {
//
//		List<GTeam> playingTeams = new ArrayList<GTeam>();
//		for(GTeam team : getTeams()){
//			if(team.isPlaying()){
//				playingTeams.add(team);
//			}
//		}
//		
//		return playingTeams;
//	}
//	
//	
//	// Methods 
//	
//	public synchronized GPlayer addPlayer(Player player){
//		GPlayer newPlayer = new GPlayer(player);
//		getPlayers().add(newPlayer);
//		return newPlayer;
//	}
//	
//	public synchronized void removePlayer(Player player){
//		removePlayer(player.getName());
//	}
//	
//	public synchronized void removePlayer(String name){
//		GPlayer gPlayer = getGPlayer(name);
//		if(gPlayer != null){
//			gPlayer.leaveTeam();
//			getPlayers().remove(gPlayer);
//		}
//	}
//	
//	public boolean isPlaying(Player player){
//		GPlayer gPlayer = getGPlayer(player);
//		if(gPlayer != null){
//			return gPlayer.getState().equals(PlayerState.PLAYING);
//		}
//		return false;
//	}
//
//	public boolean isPlayerAllowedToJoin(Player player){
//		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
//		GameManager gm = GameManager.instance();
//		
//		switch(gm.getState()){
//				
//			case WAITING:
//			case PLAYING:
//				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
//				return true;
//			case STARTING:
//			case LOADING:
//			case ENDED:
//			default:
//				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
//				return false;
//		}
//	}
//
//	public void playerJoinsTheGame(Player player) {
//		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
//		GPlayer gPlayer = getGPlayer(player);
//		
//		if(gPlayer == null){
//			gPlayer = addPlayer(player);
//		}
//		
//		GameState gameState = GameManager.instance().getState();
//		switch(gameState){
//			case WAITING:
//				gPlayer.setState(PlayerState.WAITING);
//				break;
//			case PLAYING:
//			case STARTING:
//			case LOADING:
//			case ENDED:
//			default:
//				gPlayer.setState(PlayerState.DEAD);
//				break;
//		}
//			
//		switch(gPlayer.getState()){
//			case WAITING:
//				Logger.debug("waitPlayer");
//				gPlayer.sendI18nMessage("player.welcome");
//				if(Config.isBountifulApiLoaded){
//					TitleManager.sendTitle(player, ChatColor.GREEN+"Gladiator", 20, 20, 20);
//				}
//				Logger.broadcast(
//					I18n.get("player.joined")
//						.replace("%player%",gPlayer.getName())
//						.replace("%count%", String.valueOf(getPlayers().size()))
//						.replace("%total%",  String.valueOf(Config.maxPlayers))
//				);
//				waitPlayer(gPlayer);
//				break;
//			case DEAD:
//			default:
//				Logger.debug("spectatePlayer");
//				spectatePlayer(gPlayer);
//				break;
//		}
//		
//		refreshVisiblePlayers();
//		gPlayer.refreshNameTag();
//	}
//
//	public void waitPlayer(GPlayer gPlayer){
//		
//		if(gPlayer.isOnline()){
//			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
//				gPlayer.sendI18nMessage("player.full");
//			}
//			Player player = gPlayer.getPlayer();
//			player.teleport(Config.lobby);
//			player.setGameMode(GameMode.ADVENTURE);
//			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
//			Effects.addPermanent(player, PotionEffectType.SPEED, 1);
//			player.setHealth(20);
//
//			
//			if(Config.isSIGLoaded){
//				SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
//			}
//			
//		}
//		
//	}
//	
//	public void startPlayer(final GPlayer gPlayer){
//		gPlayer.setState(PlayerState.PLAYING);
//		gPlayer.teleportToSpawnPoint();
//		UpdateScoreboardThread.add(gPlayer);
//		if(gPlayer.isOnline()){
//			Player player = gPlayer.getPlayer();
//			player.setGameMode(GameMode.SURVIVAL);
//			player.setFireTicks(0);
//			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 2, 2);
//			Effects.clear(player);
//			PlayerClassManager.instance().giveClassItems(gPlayer);
//		}
//	}
//	
//	public void spectatePlayer(GPlayer gPlayer){
//
//		gPlayer.setState(PlayerState.DEAD);
//		gPlayer.sendI18nMessage("player.spectate");
//		
//		if(gPlayer.isOnline()){
//			Player player = gPlayer.getPlayer();
//			Inventories.clear(player);
//			player.setGameMode(GameMode.SPECTATOR);
//			player.teleport(Config.gladiatorLocation);
//		}
//	}
//	
//	public List<GPlayer> getPlayingPlayers() {
//		List<GPlayer> playingPlayers = new ArrayList<GPlayer>();
//		for(GPlayer p : getPlayers()){
//			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
//				playingPlayers.add(p);
//			}
//		}
//		return playingPlayers;
//	}
//	
//	public List<GPlayer> getWaitingPlayers() {
//		List<GPlayer> waitingPlayers = new ArrayList<GPlayer>();
//		for(GPlayer p : getPlayers()){
//			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
//				waitingPlayers.add(p);
//			}
//		}
//		return waitingPlayers;
//	}
//
//	public void startAllPlayers() {
//		Logger.debug("-> PlayersManager::startAllPlayers");
//		assignRandomTeamsToPlayers();
//		refreshVisiblePlayers();
//		for(GPlayer gPlayer : getPlayers()){
//			if(gPlayer.getTeam() == null){
//				spectatePlayer(gPlayer);
//			}else{
//				startPlayer(gPlayer);
//			}
//		}
//		UpdateScoreboardThread.start();
//		Logger.debug("<- PlayersManager::startAllPlayers");
//	}
//	
//	private void refreshVisiblePlayers() {
//		for(Player player : Bukkit.getOnlinePlayers()){
//			for(Player onePlayer : Bukkit.getOnlinePlayers()){
//				player.hidePlayer(onePlayer);
//				player.showPlayer(onePlayer);
//			}
//		}
//	}
//	
//	private void assignRandomTeamsToPlayers(){
//		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
//		
//		synchronized(players){
//			// Shuffle players who will play (up to maxPlayers)
//			List<GPlayer> playersWhoWillPlay = new ArrayList<GPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
//			List<GPlayer> playersWhoWillNotPlay = new ArrayList<GPlayer>();
//			if(players.size() > Config.maxPlayers){
//				playersWhoWillNotPlay = new ArrayList<GPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
//			}
//			this.players = new ArrayList<GPlayer>();
//			Collections.shuffle(playersWhoWillPlay);
//			this.players.addAll(playersWhoWillPlay);
//			this.players.addAll(playersWhoWillNotPlay);
//		}
//		
//		// Assign gladiator
//		GPlayer gPlayerGladiator = getWaitingPlayers().get(0);
//		getTeam(TeamType.GLADIATOR).addPlayer(gPlayerGladiator);
//		gPlayerGladiator.setPlayerClass(PlayerClassManager.instance().getGladiator());
//		Player gladiator = gPlayerGladiator.getPlayer();
//		if(Config.isBountifulApiLoaded)
//			TitleManager.sendTitle(gladiator, I18n.get("player.gladiator",gladiator), 10, 20 , 10);
//		else
//			gPlayerGladiator.sendI18nMessage("player.gladiator");
//		
//		
//		GTeam slaveTeam = getTeam(TeamType.SLAVES);
//		
//		int nPlayer = 1;
//		for(GPlayer gPlayer : getWaitingPlayers()){
//			
//			nPlayer++;
//			if(nPlayer <= Config.maxPlayers){
//				
//				if(gPlayer.getTeam() == null){
//					slaveTeam.addPlayer(gPlayer);
//					gPlayer.setPlayerClass(PlayerClassManager.instance().getSlave());
//					
//					Player slave = gPlayer.getPlayer();
//					if(Config.isBountifulApiLoaded)
//						TitleManager.sendTitle(slave, I18n.get("player.slave",slave), 10, 20 , 10);
//					else
//						gPlayer.sendI18nMessage("player.slave");
//						
//					Logger.debug("player="+gPlayer.getName()+", team="+gPlayer.getTeam().getType()+", spawn="+Locations.printLocation(gPlayer.getSpawnPoint()));
//				}
//
//			}
//			
//			gPlayer.refreshNameTag();
//		}
//
//		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
//	}
//
//	public void endAllPlayers(EndCause cause, GTeam winningTeam) {
//		
//		if(winningTeam != null){
//			for(GPlayer gPlayer : winningTeam.getMembers()){
//				if(gPlayer.isOnline()){
//					gPlayer.addMoney(VaultManager.addMoney(gPlayer.getPlayer(), Config.winReward));
//				}
//			}
//		}
//
//		for(GPlayer gPlayer : getPlayers()){
//			if(gPlayer.isOnline()){
//				spectatePlayer(gPlayer);
//				printEndMessage(gPlayer, winningTeam);
//			}
//		}
//
//		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
//		
//	}
//	
//	private void printEndMessage(GPlayer rPlayer, GTeam winningTeam){
//		if(rPlayer.isOnline() && winningTeam != null){
//			Player player = rPlayer.getPlayer();
//			Texts.tellraw(player, I18n.get("stats.end",player)
//					.replace("%kills%", String.valueOf(rPlayer.getKills()))
//					.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
//					.replace("%winner%", winningTeam.getColor()+winningTeam.getI18nName(player))
//			);
//		}
//	}
//	
//}
