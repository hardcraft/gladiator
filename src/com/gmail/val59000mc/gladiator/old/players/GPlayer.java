//package com.gmail.val59000mc.gladiator.old.players;
//
//
//import org.bukkit.Bukkit;
//import org.bukkit.ChatColor;
//import org.bukkit.Location;
//import org.bukkit.entity.Player;
//
//import ca.wacos.nametagedit.NametagAPI;
//
//import com.gmail.val59000mc.gladiator.old.classes.PlayerClass;
//import com.gmail.val59000mc.gladiator.old.classes.PlayerClassType;
//import com.gmail.val59000mc.gladiator.old.dependencies.PermissionsExManager;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.simpleinventorygui.spigotutils.Numbers;
//import com.gmail.val59000mc.spigotutils.Logger;
//
//public class GPlayer {
//	private String name;
//	private GTeam team;
//	private PlayerState state;
//	private boolean globalChat;
//	private PlayerClass playerClass;
//	private int kills;
//	private double money;
//	private Location spawnpoint;
//	
//	
//	// Constructor
//	
//	public GPlayer(Player player){
//		this.name = player.getName();
//		this.team = null;
//		this.state = PlayerState.WAITING;
//		this.globalChat = true;
//		this.playerClass = null;
//		this.kills = 0;
//		this.money = 0;
//		this.spawnpoint = null;
//	}
//	
//	
//	
//	// Accessors
//
//	public String getName() {
//		return name;
//	}
//	public Location getSpawnPoint() {
//		return spawnpoint;
//	}
//	public void setSpawnPoint(Location location) {
//		this.spawnpoint = location;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	
//	public synchronized GTeam getTeam() {
//		return team;
//	}
//	public synchronized void setTeam(GTeam team) {
//		this.team = team;
//	}
//	public PlayerState getState() {
//		return state;
//	}
//	public void setState(PlayerState state) {
//		this.state = state;
//	}
//	public double getMoney() {
//		return Numbers.round(money,2);
//	}
//	public void addMoney(double money) {
//		this.money += money;
//	}
//	public boolean isGlobalChat() {
//		return globalChat;
//	}
//	public void setGlobalChat(boolean globalChat) {
//		this.globalChat = globalChat;
//	}
//	public PlayerClass getPlayerClass() {
//		return playerClass;
//	}
//	public void setPlayerClass(PlayerClass playerClass) {
//		this.playerClass = playerClass;
//	}
//	public int getKills() {
//		return kills;
//	}
//	public void addKill() {
//		this.kills++;
//	}
//	
//	// Methods
//
//	public ChatColor getColor(){
//		if(getTeam() != null){
//			return getTeam().getColor();
//		}else{
//			return ChatColor.WHITE;
//		}
//	}
//	public Player getPlayer(){
//		return Bukkit.getPlayer(name);
//	}
//	
//	public Boolean isOnline(){
//		return Bukkit.getPlayer(name) != null;
//	}
//	
//	public boolean isInTeamWith(GPlayer player){
//		return (team != null && team.equals(player.getTeam()));
//	}
//	
//	public void leaveTeam(){
//		if(team != null){
//			team.leave(this);
//			team = null;
//		}
//	}
//
//	public void sendI18nMessage(String code) {
//		if(isOnline()){
//			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
//		}
//	}
//	
//	
//	public String toString(){
//		return "[name='"+getName()+
//				"',team='"+((getTeam() == null) ? null : getTeam().getType())+"'"+
//				",class='"+((getPlayerClass() == null) ? null : getPlayerClass().getType())+"']";
//	}
//
//
//
//	public boolean isClass(PlayerClassType type) {
//		return getPlayerClass() != null && getPlayerClass().getType().equals(type);
//	}
//
//
//
//	public boolean isTeam(TeamType type) {
//		return getTeam() != null && getTeam().getType().equals(type);
//	}
//
//
//
//	public void teleportToSpawnPoint() {
//		if(isOnline() && getSpawnPoint() != null){
//			getPlayer().teleport(getSpawnPoint());
//		}
//	}
//
//
//
//	public boolean isState(PlayerState state) {
//		return getState().equals(state);
//	}
//	
//	public void refreshNameTag(){
//		String prefix = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', PermissionsExManager.getShortPrefix(getPlayer())));
//
//		
//		String suffix = "&f";
//		
//		if(getTeam() != null){
//			switch(getTeam().getType()){
//				case GLADIATOR:
//					suffix = "&c";
//					break;
//				case SLAVES:
//					suffix = "&a";
//					break;
//			}
//			
//		}
//		
//		NametagAPI.setPrefix(getName(), "&7"+prefix+suffix);
//	}
//
//
//
//	public boolean isPlaying() {
//		return isState(PlayerState.PLAYING) && isOnline();
//	}
//
//
//}
