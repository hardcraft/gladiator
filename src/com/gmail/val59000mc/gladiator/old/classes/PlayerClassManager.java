//package com.gmail.val59000mc.gladiator.old.classes;
//
//import java.lang.reflect.InvocationTargetException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.bukkit.configuration.ConfigurationSection;
//import org.bukkit.inventory.ItemStack;
//
//import com.gmail.val59000mc.gladiator.Gladiator;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.spigotutils.Logger;
//import com.gmail.val59000mc.spigotutils.Parser;
//import com.gmail.val59000mc.spigotutils.Reflection;
//
//public class PlayerClassManager {
//	
//	
//	private static PlayerClassManager instance;
//	private PlayerClass gladiator;
//	private PlayerClass slave;
//
//	public static PlayerClassManager instance(){
//		if(instance == null){
//			instance = new PlayerClassManager();
//		}
//		
//		return instance;
//	}
//
//	public PlayerClass getGladiator() {
//		return gladiator;
//	}
//
//	public PlayerClass getSlave() {
//		return slave;
//	}
//
//	private PlayerClassManager(){
//		this.gladiator = null;
//		this.slave = null;
//	}
//	
//	public static void load(){
//		
//		PlayerClassManager instance = PlayerClassManager.instance();
//
//		// defense
//		instance.gladiator = instance.parseClass(PlayerClassType.GLADIATOR);
//		instance.slave = instance.parseClass(PlayerClassType.SLAVE);
//	
//	}
//	
//	private PlayerClass parseClass(PlayerClassType type){
//		Logger.debug("-> PlayerClass::parseWarriorClass, type="+type);
//		
////		ConfigurationSection section = Gladiator.getPlugin().getConfig().getConfigurationSection("stuff."+type.toString());
//		
//		if(section == null){
//			Logger.severeC(I18n.get("parser.playerclass.not-found"));
//			return null;
//		}
//		
//		List<ItemStack> swords = parseItemList(section.getStringList("swords"));
//		List<ItemStack> bows = parseItemList(section.getStringList("bows"));
//		List<ItemStack> potions = parseItemList(section.getStringList("potions"));
//		List<ItemStack> helmets = parseItemList(section.getStringList("helmets"));
//		List<ItemStack> chestplates = parseItemList(section.getStringList("chestplates"));
//		List<ItemStack> leggings = parseItemList(section.getStringList("leggings"));
//		List<ItemStack> boots = parseItemList(section.getStringList("boots"));
//		List<ItemStack> extras = parseItemList(section.getStringList("extras"));
//		
//		PlayerClass playerClass;
//		
//		try {
//			playerClass = (PlayerClass) Reflection.instantiateObject(Class.forName(type.getClassName()), 
//					type,
//					swords, 
//					bows,
//					potions, 
//					helmets, 
//					chestplates,
//					leggings, 
//					boots,
//					extras);
//			Logger.debug("<- PlayerClass::parseClass");
//			return playerClass;
//		} catch (InstantiationException | IllegalAccessException
//				| IllegalArgumentException | InvocationTargetException
//				| NoSuchMethodException | ClassNotFoundException e) {
//			Logger.severeC(I18n.get("parser.playerclass.wrong-player-class"));			
//			Logger.debug("<- PlayerClass::parseClass");
//			e.printStackTrace();
//			return null;
//		}
//		
//
//	}
//	
//	private List<ItemStack> parseItemList(List<String> itemsStr){
//		List<ItemStack> items = new ArrayList<ItemStack>();
//		if(itemsStr == null){
//			Logger.severeC(I18n.get("parser.playerclass.items-not-found"));
//			return null;
//		}
//		for(String itemStr : itemsStr){
//			//items.add(Parser.parseItemStack(itemStr));
//		}
//		return items;
//	}
//
//	public void giveClassItems(GPlayer gPlayer) {
//		if(gPlayer.getPlayerClass() != null){
//			gPlayer.getPlayerClass().applyLogic(gPlayer);
//		}
//	}
//}
