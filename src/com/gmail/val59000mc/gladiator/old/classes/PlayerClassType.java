package com.gmail.val59000mc.gladiator.old.classes;

public enum PlayerClassType {
	GLADIATOR("com.gmail.val59000mc.gladiator.classes.Gladiator"),
	SLAVE("com.gmail.val59000mc.gladiator.classes.Slave");
	
	private String className;

    private PlayerClassType(String className){
    	this.className = className;
    }

    public String getClassName() {
        return className;
    }
}
