//package com.gmail.val59000mc.gladiator.old.classes;
//
//import java.util.List;
//
//import org.bukkit.Material;
//import org.bukkit.entity.Player;
//import org.bukkit.inventory.ItemStack;
//
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.spigotutils.Inventories;
//
//
//public class Gladiator extends PlayerClass{
//
//	
//
//
//	public Gladiator(PlayerClassType type, List<ItemStack> swords,
//			List<ItemStack> bows, List<ItemStack> potions,
//			List<ItemStack> helmets, List<ItemStack> chestplates,
//			List<ItemStack> leggings, List<ItemStack> boots,
//			List<ItemStack> extras) {
//		super(type, swords, bows, potions, helmets, chestplates, leggings, boots,
//				extras);
//	}
//
//	@Override
//	public void applyLogic(GPlayer gPlayer) {
//		super.applyLogic(gPlayer);
//		if(gPlayer.isOnline()){
//			Player player = gPlayer.getPlayer();
//			int hearts = getHearts(player);
//			player.setMaxHealth(hearts);
//			player.setHealth(hearts);
//			Inventories.give(player, getRandomItem(swords));
//			Inventories.give(player, getRandomItem(bows));
//			Inventories.give(player, getRandomItem(potions));
//			Inventories.give(player, getRandomItem(extras));
//			Inventories.give(player, new ItemStack(Material.ARROW, 40));
//			Inventories.give(player, new ItemStack(Material.COOKED_BEEF, 15));
//		}
//		
//	}
//	
//	private int getHearts(Player player){
//		int hearts = 40;
//		if(player.hasPermission("hardcraftpvp.gladiator.heart.60")){
//			hearts = 60;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.58")){
//			hearts = 58;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.56")){
//			hearts = 56;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.54")){
//			hearts = 54;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.52")){
//			hearts = 52;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.50")){
//			hearts = 50;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.48")){
//			hearts = 48;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.46")){
//			hearts = 46;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.44")){
//			hearts = 44;
//		}else if(player.hasPermission("hardcraftpvp.gladiator.heart.42")){
//			hearts = 42;
//		}
//		return hearts;
//	}
//
//}
