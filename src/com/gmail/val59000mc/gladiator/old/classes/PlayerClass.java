//package com.gmail.val59000mc.gladiator.old.classes;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.bukkit.entity.Player;
//import org.bukkit.inventory.ItemStack;
//
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.spigotutils.Inventories;
//import com.gmail.val59000mc.spigotutils.Randoms;
//
//public abstract class PlayerClass {
//	private PlayerClassType type;
//	protected List<ItemStack> swords;
//	protected List<ItemStack> bows;
//	protected List<ItemStack> potions;
//	protected List<ItemStack> helmets;
//	protected List<ItemStack> chestplates;
//	protected List<ItemStack> leggings;
//	protected List<ItemStack> boots;
//	protected List<ItemStack> extras;
//	
//	public PlayerClass(PlayerClassType type, 
//			List<ItemStack> swords, 
//			List<ItemStack> bows, 
//			List<ItemStack> potions, 
//			List<ItemStack> helmets, 
//			List<ItemStack> chestplates, 
//			List<ItemStack> leggings, 
//			List<ItemStack> boots,
//			List<ItemStack> extras) {
//		super();
//		this.type = type;
//		this.swords = swords;
//		this.bows = bows;
//		this.potions = potions;
//		this.helmets = helmets;
//		this.chestplates =chestplates;
//		this.leggings = leggings;
//		this.boots = boots;
//		this.extras = extras;
//	}
//
//	public PlayerClassType getType() {
//		return type;
//	}
//	
//	public void applyLogic(GPlayer wicPlayer){
//		if(wicPlayer.isOnline()){
//			Player player = wicPlayer.getPlayer();
//			Inventories.clear(player);
//			Inventories.setArmor(player, getRandomArmor());
//		}
//	}
//	
//	private List<ItemStack> getRandomArmor(){
//		List<ItemStack> armor = new ArrayList<ItemStack>();
//		armor.add(getRandomItem(helmets));
//		armor.add(getRandomItem(chestplates));
//		armor.add(getRandomItem(leggings));
//		armor.add(getRandomItem(boots));
//		return armor;
//		
//	}
//	
//	protected ItemStack getRandomItem(List<ItemStack> items){
//		return ((items == null || items.isEmpty() ) ? null : items.get(Randoms.randomInteger(0, items.size()-1)));
//	}
//}
