//package com.gmail.val59000mc.gladiator.old.classes;
//
//import java.util.List;
//
//import org.bukkit.Material;
//import org.bukkit.entity.Player;
//import org.bukkit.inventory.ItemStack;
//
//import com.gmail.val59000mc.gladiator.old.players.GPlayer;
//import com.gmail.val59000mc.spigotutils.Inventories;
//import com.gmail.val59000mc.spigotutils.Randoms;
//
//
//public class Slave extends PlayerClass{
//
//
//	public Slave(PlayerClassType type, List<ItemStack> swords,
//			List<ItemStack> bows, List<ItemStack> potions,
//			List<ItemStack> helmets, List<ItemStack> chestplates,
//			List<ItemStack> leggings, List<ItemStack> boots,
//			List<ItemStack> extras) {
//		super(type, swords, bows, potions, helmets, chestplates, leggings, boots,
//				extras);
//	}
//
//	@Override
//	public void applyLogic(GPlayer gPlayer) {
//		super.applyLogic(gPlayer);
//		if(gPlayer.isOnline()){
//			Player player = gPlayer.getPlayer();
//			player.setMaxHealth(20);
//			player.setHealth(20);
//			
//
//			Inventories.give(player, getRandomItem(swords));
//			
//			boolean giveBow = Randoms.randomInteger(1, 5) <= getLootChance(player,"bow");
//			if(giveBow){
//				Inventories.give(player, getRandomItem(bows));
//				Inventories.give(player, new ItemStack(Material.ARROW, 15));
//			}
//			
//			boolean giveExtra = Randoms.randomInteger(1, 5) <= getLootChance(player,"extra");
//			if(giveExtra){
//				Inventories.give(player, getRandomItem(extras));
//			}
//			
//			boolean givePotion = Randoms.randomInteger(1, 5) <= getLootChance(player,"potion");
//			if(givePotion){
//				Inventories.give(player, getRandomItem(potions));
//			}
//			
//			Inventories.give(player, new ItemStack(Material.COOKED_BEEF, 15));
//		}
//	}
//	
//	private int getLootChance(Player player, String node){
//		int chance = 1;
//		if(player.hasPermission("hardcraftpvp.gladiator."+node+".5")){
//			chance = 5;
//		}else if(player.hasPermission("hardcraftpvp.gladiator."+node+".4")){
//			chance = 4;
//		}else if(player.hasPermission("hardcraftpvp.gladiator."+node+".3")){
//			chance = 3;
//		}else if(player.hasPermission("hardcraftpvp.gladiator."+node+".2")){
//			chance = 2;
//		}
//		return chance;
//	}
//
//}
