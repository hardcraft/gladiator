//package com.gmail.val59000mc.gladiator.old.game;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.bukkit.Bukkit;
//import org.bukkit.event.Listener;
//
//import com.gmail.val59000mc.gladiator.Gladiator;
//import com.gmail.val59000mc.gladiator.old.classes.PlayerClassManager;
//import com.gmail.val59000mc.gladiator.old.commands.StartCommand;
//import com.gmail.val59000mc.gladiator.old.configuration.Config;
//import com.gmail.val59000mc.gladiator.old.i18n.I18n;
//import com.gmail.val59000mc.gladiator.old.listeners.BlockListener;
//import com.gmail.val59000mc.gladiator.old.listeners.IceListener;
//import com.gmail.val59000mc.gladiator.old.listeners.PingListener;
//import com.gmail.val59000mc.gladiator.old.listeners.PlayerChatListener;
//import com.gmail.val59000mc.gladiator.old.listeners.PlayerConnectionListener;
//import com.gmail.val59000mc.gladiator.old.listeners.PlayerDamageListener;
//import com.gmail.val59000mc.gladiator.old.listeners.PlayerDeathListener;
//import com.gmail.val59000mc.gladiator.old.maploader.MapLoader;
//import com.gmail.val59000mc.gladiator.old.players.GTeam;
//import com.gmail.val59000mc.gladiator.old.players.PlayersManager;
//import com.gmail.val59000mc.gladiator.old.threads.CheckRemainingPlayerThread;
//import com.gmail.val59000mc.gladiator.old.threads.PreventFarAwayPlayerThread;
//import com.gmail.val59000mc.gladiator.old.threads.RestartThread;
//import com.gmail.val59000mc.gladiator.old.threads.UpdateScoreboardThread;
//import com.gmail.val59000mc.gladiator.old.threads.WaitForNewPlayersThread;
//import com.gmail.val59000mc.spigotutils.Logger;
//
//
//public class GameManager {
//
//	private static GameManager instance = null;
//	
//	private GameState state;
//	private boolean pvp;
//	
//	// static
//	
//	public static GameManager instance(){
//		if(instance == null){
//			instance = new GameManager();
//		}
//		return instance;
//	}
//
//	
//	// constructor 
//	
//	private GameManager(){
//		this.state = GameState.LOADING;
//		this.pvp = false;
//	}
//
//	
//	// accessors
//	
//	public GameState getState() {
//		return state;
//	}
//	
//	
//
//	public boolean isPvp() {
//		return pvp;
//	}
//
//
//	public boolean isState(GameState state) {
//		return getState().equals(state);
//	}
//	
//	// methods 
//	
//	public void loadGame() {
//		state = GameState.LOADING;
//		Logger.debug("-> GameManager::loadNewGame");
//		Config.load();
//		PlayerClassManager.load();
//		
//		MapLoader.deleteOldPlayersFiles();
//		MapLoader.load();
//		
//		registerListeners();
//		registerCommands();
//		
//		if(Config.isBungeeEnabled)
//			Gladiator.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(Gladiator.getPlugin(), "BungeeCord");
//
//		waitForNewPlayers();
//		Logger.debug("<- GameManager::loadNewGame");
//	}
//
//
//	
//	private void registerListeners(){
//		Logger.debug("-> GameManager::registerListeners");
//		// Registers Listeners
//		List<Listener> listeners = new ArrayList<Listener>();		
//		listeners.add(new PlayerConnectionListener());	
//		listeners.add(new PlayerChatListener());
//		listeners.add(new PlayerDamageListener());
//		listeners.add(new PlayerDeathListener());
//		listeners.add(new IceListener());
//		listeners.add(new PingListener());
//		listeners.add(new BlockListener());
//		for(Listener listener : listeners){
//			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
//			Bukkit.getServer().getPluginManager().registerEvents(listener, Gladiator.getPlugin());
//		}
//		Logger.debug("<- GameManager::registerListeners");
//	}
//
//	
//	private void registerCommands(){
//		Logger.debug("-> GameManager::registerCommands");
//		// Registers Listeners	
//		Gladiator.getPlugin().getCommand("start").setExecutor(new StartCommand());
//		Logger.debug("<- GameManager::registerCommands");
//	}
//
//
//
//	private void waitForNewPlayers(){
//		Logger.debug("-> GameManager::waitForNewPlayers");
//		WaitForNewPlayersThread.start();
//		PreventFarAwayPlayerThread.start(Config.lobbyBounds);
//		UpdateScoreboardThread.start();
//		state = GameState.WAITING;
//		Logger.infoC(I18n.get("game.player-allowed-to-join"));
//		Logger.debug("<- GameManager::waitForNewPlayers");
//	}
//	
//	public void startGame(){
//		Logger.debug("-> GameManager::startGame");
//		state = GameState.STARTING;
//		Logger.broadcast(I18n.get("game.start"));
//		PreventFarAwayPlayerThread.stop();
//		PlayersManager.instance().startAllPlayers();	
//		playGame();
//		Logger.debug("<- GameManager::startGame");
//	}
//	
//	private void playGame(){
//		state = GameState.PLAYING;
//		pvp = true;
//		CheckRemainingPlayerThread.start();
//		PreventFarAwayPlayerThread.start(Config.bounds);
//	}
//	
//	public void endGame(EndCause cause, GTeam winningTeam) {
//		if(state.equals(GameState.PLAYING)){
//			state = GameState.ENDED;
//			pvp = false;
//			CheckRemainingPlayerThread.stop();
//			PreventFarAwayPlayerThread.stop();
//			PlayersManager.instance().endAllPlayers(cause,winningTeam);
//			RestartThread.start();
//		}
//		
//	}
//
//	
//	
//
//
//	
//	
//	
//
//}
