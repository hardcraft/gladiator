package com.gmail.val59000mc.gladiator.old.game;

public enum GameState {
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED;
}
