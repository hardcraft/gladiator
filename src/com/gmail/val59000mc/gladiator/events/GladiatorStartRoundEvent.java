package com.gmail.val59000mc.gladiator.events;

import com.gmail.val59000mc.gladiator.rounds.Round;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;

public class GladiatorStartRoundEvent extends HCEvent{
	
	Round round;
	
	public GladiatorStartRoundEvent(HCGameAPI api, Round round) {
		super(api);
		this.round = round;
	}

	public Round getRound(){
		return round;
	}

}
